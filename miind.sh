#! /bin/bash

# If permission is denied, run this:
# chmod +x cloudii.sh

# Get the arguments
type=$1
command=$2
board=$3

if [[ $type == 'pio' ]]; then
  if [[ $command == 'run' ]]; then
    echo "Executing platformio run command..."
    if [[ $board == 'uno' ]]; then
      echo "Compiling code for arduino uno..."
      cd platformio/
      pio run -e uno
      cd ..
    elif [[ $board == 'nano' ]]; then
      echo "Compiling code for arduino nano..."
      cd platformio/
      pio run -e nanoatmega328
      cd ..
    fi
  elif [[ $command == 'upload' ]]; then
    echo "Executing platformio upload command..."
    if [[ $board == 'uno' ]]; then
      echo "Uploading code for arduino uno..."
      cd platformio/
      pio run -e uno --target upload
      cd ..
    elif [[ $board == 'nano' ]]; then
      echo "Uploading code for arduino nano..."
      cd platformio/
      pio run -e nanoatmega328 --target upload
      cd ..
    fi
  elif [[ $command == 'monitor' ]]; then
    echo "Executing platformio device monitor..."
    pio device monitor    
  fi
elif [[ $type == 'sync' ]]; then
  echo "Nothing here"
elif [[ $type == 'main' ]]; then
  echo "Running main codes..."
  if [[ $command == 'am' ]]; then
    echo "Executing AM comms..."
    if [[ $board == 'plutosdr' ]]; then
      echo "Compiling code for arduino uno..."
      cd platformio/
      pio run -e uno
      cd ..
    elif [[ $board == 'osmosdr' ]]; then
      echo "Compiling code for arduino nano..."
      cd platformio/
      pio run -e nanoatmega328
      cd ..
    fi
  elif [[ $command == 'fm' ]]; then
    echo "Executing platformio upload command..."
    if [[ $board == 'uno' ]]; then
      echo "Uploading code for arduino uno..."
      cd platformio/
      pio run -e uno --target upload
      cd ..
    elif [[ $board == 'nano' ]]; then
      echo "Uploading code for arduino nano..."
      cd platformio/
      pio run -e nanoatmega328 --target upload
      cd ..
    fi
  elif [[ $command == 'monitor' ]]; then
    echo "Executing platformio device monitor..."
    pio device monitor    
  fi
fi