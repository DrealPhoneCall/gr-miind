INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_MIIND miind)

FIND_PATH(
    MIIND_INCLUDE_DIRS
    NAMES miind/api.h
    HINTS $ENV{MIIND_DIR}/include
        ${PC_MIIND_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    MIIND_LIBRARIES
    NAMES gnuradio-miind
    HINTS $ENV{MIIND_DIR}/lib
        ${PC_MIIND_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MIIND DEFAULT_MSG MIIND_LIBRARIES MIIND_INCLUDE_DIRS)
MARK_AS_ADVANCED(MIIND_LIBRARIES MIIND_INCLUDE_DIRS)

