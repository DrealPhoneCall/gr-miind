#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2019 Daryl Pongcol.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy
import serial
from gnuradio import gr


class arduino_multichannel_serial_src_f(gr.sync_block):
    """
    The block that takes data from arduino via the firmata protocol.

    """
    def __init__(self, serial_port, baud_rate, samp_rate, num_inputs):        
        self.serial_port = serial_port
        self.baud_rate = baud_rate
        self.samp_rate = samp_rate
        self.num_inputs = num_inputs

        # init gr.sync_block.__init__()
        out_sig = self.get_out_sig()
        gr.sync_block.__init__(self, name="arduino_multichannel_serial_src_f",
                               in_sig=[], out_sig=out_sig)

        # start arduino firmata object
        self.init_serial()

    def init_serial(self):
        try:
            self.serial = serial.Serial(self.serial_port, self.baud_rate)
        except Exception as e:
            print e
            self.serial = None

    def get_out_sig(self):
        return [numpy.float32 for i in range(self.num_inputs)]

    def set_num_inputs(self, num_inputs):
        self.num_inputs = num_inputs
        self.init_sync_block_inout_sig()

    def get_time_pause(self):
        print "samp_rate =", self.samp_rate
        return 1 / self.samp_rate
        # return 0.0001

    def set_serial_port(self, serial_port):
        self.serial_port = serial_port

    def work(self, input_items, output_items):
        data_out = output_items

        # print "len(data_out) =", len(data_out)
        # print "data_out ="
        # print data_out

        time_pause = self.get_time_pause()
        # print "time_pause =", time_pause

        for j in range(0, len(data_out[0])):
            for i in range(0, self.num_inputs):
                data_out[i][j] = float(self.serial.readline())
                # print "data_out[%d][%d] = %d" % (i, j, data_out[i][j]) 

        return len(data_out[0])

