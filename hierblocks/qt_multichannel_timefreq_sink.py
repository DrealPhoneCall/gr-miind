#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Multichannel Time Freq Sink
# Author: Daryl Pongcol
# Description: Multiple channel Qt time and freq sink in one widget
# Generated: Mon Oct 22 13:25:35 2018
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import wxgui
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from gnuradio.wxgui import fftsink2
from gnuradio.wxgui import scopesink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx


class qt_multichannel_timefreq_sink(grc_wxgui.top_block_gui):

    def __init__(self, bandwidth=100e3, center_freq=50e3, fft_size=1024, num_points=1024, samp_rate=1e6, update_period=0.10):
        grc_wxgui.top_block_gui.__init__(self, title="Multichannel Time Freq Sink")
        _icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
        self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

        ##################################################
        # Parameters
        ##################################################
        self.bandwidth = bandwidth
        self.center_freq = center_freq
        self.fft_size = fft_size
        self.num_points = num_points
        self.samp_rate = samp_rate
        self.update_period = update_period

        ##################################################
        # Blocks
        ##################################################
        self.wxgui_scopesink2_0 = scopesink2.scope_sink_f(
        	self.GetWin(),
        	title="Scope Plot",
        	sample_rate=samp_rate,
        	v_scale=0,
        	v_offset=0,
        	t_scale=0,
        	ac_couple=False,
        	xy_mode=False,
        	num_inputs=8,
        	trig_mode=wxgui.TRIG_MODE_AUTO,
        	y_axis_label="Points",
        )
        self.GridAdd(self.wxgui_scopesink2_0.win, 0, 0, 4, 12)
        self.wxgui_fftsink2_0 = fftsink2.fft_sink_f(
        	self.GetWin(),
        	baseband_freq=center_freq,
        	y_per_div=10,
        	y_divs=10,
        	ref_level=0,
        	ref_scale=2.0,
        	sample_rate=samp_rate,
        	fft_size=fft_size,
        	fft_rate=15,
        	average=False,
        	avg_alpha=None,
        	title="FFT Plot",
        	peak_hold=False,
        )
        self.GridAdd(self.wxgui_fftsink2_0.win, 4, 0, 4, 12)

        ##################################################
        # Connections
        ##################################################
        self.connect((self, 0), (self.wxgui_fftsink2_0, 0))    
        self.connect((self, 0), (self.wxgui_scopesink2_0, 0))    
        self.connect((self, 1), (self.wxgui_scopesink2_0, 1))    
        self.connect((self, 2), (self.wxgui_scopesink2_0, 2))    
        self.connect((self, 3), (self.wxgui_scopesink2_0, 3))    
        self.connect((self, 4), (self.wxgui_scopesink2_0, 4))    
        self.connect((self, 5), (self.wxgui_scopesink2_0, 5))    
        self.connect((self, 6), (self.wxgui_scopesink2_0, 6))    
        self.connect((self, 7), (self.wxgui_scopesink2_0, 7))    

    def get_bandwidth(self):
        return self.bandwidth

    def set_bandwidth(self, bandwidth):
        self.bandwidth = bandwidth

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.wxgui_fftsink2_0.set_baseband_freq(self.center_freq)

    def get_fft_size(self):
        return self.fft_size

    def set_fft_size(self, fft_size):
        self.fft_size = fft_size

    def get_num_points(self):
        return self.num_points

    def set_num_points(self, num_points):
        self.num_points = num_points

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.wxgui_scopesink2_0.set_sample_rate(self.samp_rate)
        self.wxgui_fftsink2_0.set_sample_rate(self.samp_rate)

    def get_update_period(self):
        return self.update_period

    def set_update_period(self, update_period):
        self.update_period = update_period


def argument_parser():
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    parser.add_option(
        "", "--bandwidth", dest="bandwidth", type="eng_float", default=eng_notation.num_to_str(100e3),
        help="Set Bandwidth [default=%default]")
    parser.add_option(
        "", "--center-freq", dest="center_freq", type="eng_float", default=eng_notation.num_to_str(50e3),
        help="Set Center Frequency [default=%default]")
    parser.add_option(
        "", "--fft-size", dest="fft_size", type="intx", default=1024,
        help="Set FFT Size [default=%default]")
    parser.add_option(
        "", "--num-points", dest="num_points", type="intx", default=1024,
        help="Set Number of Points [default=%default]")
    parser.add_option(
        "", "--samp-rate", dest="samp_rate", type="eng_float", default=eng_notation.num_to_str(1e6),
        help="Set Sample Rate [default=%default]")
    parser.add_option(
        "", "--update-period", dest="update_period", type="eng_float", default=eng_notation.num_to_str(0.10),
        help="Set Update Period [default=%default]")
    return parser


def main(top_block_cls=qt_multichannel_timefreq_sink, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(bandwidth=options.bandwidth, center_freq=options.center_freq, fft_size=options.fft_size, num_points=options.num_points, samp_rate=options.samp_rate, update_period=options.update_period)
    tb.Start(True)
    tb.Wait()


if __name__ == '__main__':
    main()
