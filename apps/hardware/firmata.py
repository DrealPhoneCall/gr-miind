#from gnuradio import iio

class firmata_source:
    def __init__(self, var):
        self.var = var
        self.mcu_source = iio.pluto_source('ip:pluto2.local', 
                                           int(var.channel_freq), 
                                           int(var.samp_rate), 
                                           int(var.rf_bandwidth), 
                                           0x8000, True, True, True, "manual", 
                                           var.rx_gain, '', True)

    def get_mcu_source(self):
        return self.mcu_source

    def set_mcu_params(self, var, params=[]):
        self.mcu_source.set_params(int(var.channel_freq), 
                                   int(var.samp_rate), 
                                   int(var.rf_bandwidth), 
                                   True, True, True, "manual", 
                                   var.rx_gain, '', True)

class firmata_sink:
    def __init__(self, var):
        self.var = var
        self.mcu_sink = iio.pluto_sink('ip:pluto2.local', 
                                       int(var.channel_freq), 
                                       int(var.samp_rate), 
                                       int(var.rf_bandwidth), 
                                       0x8000, False, 
                                       var.tx_attenuation, '', True)

    def get_mcu_sink(self):
        return self.mcu_sink

    def set_mcu_params(self, var, params=[]):
        self.mcu_sink.set_params(int(var.channel_freq), 
                                int(var.samp_rate), 
                                int(var.rf_bandwidth), 
                                var.tx_attenuation, '', True)



    

    
