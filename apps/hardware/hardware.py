from apps.hardware.arduino import arduino_source, arduino_sink 
from apps.hardware.firmata import firmata_source, firmata_sink 

class hw_source:
    def __init__(self, var, hardware):
        self.var = var
        self.hardware = hardware

        if self.hardware in ['arduino_uno', 'arduino_nano']:
            self.hw_source_class = arduino_source(var)
        elif self.hardware in ['firmata', 'arduino_firmata']:
            self.hw_source_class = firmata_source(var)
        elif self.hardware == 'fpga':
            pass
        elif self.hardware == 'gpio':
            pass
        elif self.hardware == 'esp':
            pass
        elif self.hardware == 'adc':
            pass
        elif self.hardware == 'dac':
            pass
        else:
            pass

        self.hw_source = self.hw_source_class.get_source()

    def get_hw_source(self):
        return self.hw_source

    def set_hw_params(self, var, params=[]):
        self.hw_source_class.set_params(var, params)

class hw_sink:
    def __init__(self, var, hardware):
        self.var = var
        self.hardware = hardware
        if self.hardware in ['arduino_uno', 'arduino_nano']:
            self.hw_sink_class = arduino_sink(var)
        elif self.hardware in ['firmata', 'arduino_firmata']:
            self.hw_sink_class = firmata_sink(var)
        elif self.hardware == 'fpga':
            pass
        elif self.hardware == 'gpio':
            pass
        elif self.hardware == 'esp':
            pass
        elif self.hardware == 'adc':
            pass
        elif self.hardware == 'dac':
            pass
        else:
            pass

        self.hw_sink = self.hw_sink_class.get_hw_sink()
        
    def get_hw_sink(self):
        return self.hw_sink

    def set_hw_params(self, var, params=[]):
        self.hw_sink_class.set_hw_params(var, params)