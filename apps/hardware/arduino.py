import miindprobe

class arduino_source():
    def __init__(self, var):
        self.var = var
        self.source = miindprobe.serial_source_f( 
            serial_port=self.var.serial_port,
            baud_rate=self.var.baud_rate,
            num_channels=self.var.num_channels
        )

    def get_source(self):
        return self.source

    def set_var(self, var):
        self.var = var

    def set_params(self, var):
        self.source.set_params(
            serial_port=var.serial_port,
            baud_rate=var.baud_rate,
            num_channels=var.num_channels
        )


class arduino_sink():
    def __init__(self, var):
        self.var = var
        self.sink = miindprobe.serial_sink(  )
        
    def set_var(self, var):
        self.var = var

    def get_sink(self):
        return self.sink

    def set_params(self, var, params=[]):
        if 'samp_rate' in params:
            self.sink.set_samp_rate(var.samp_rate)
        if 'channel_freq' in params:
            self.sink.set_center_freq(var.channel_freq)
        if 'tx_gain' in params:
            self.sink.set_gain(var.tx_gain)
            self.sink.set_if_gain(var.tx_gain)
            self.sink.set_bb_gain(var.tx_gain)
        if 'rf_bandwidth' in params:
            self.sink.set_bandwidth(var.rf_bandwidth)



    

    