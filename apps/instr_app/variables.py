import os
import sys
import random
import json
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from PyQt4 import Qt
from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import qtgui

from apps.instr_app.utils import get_or_create_dump_file

class mprobe_variables():
    def __init__(self, json_file='default.json'):
        self.json_file = json_file
        self.json = self.get_param_json()
        self.serial_comm = self.json['serial_comm']
        self.serial_port = self.json['serial_port']
        self.baud_rate = self.json['baud_rate']
        self.update_period = self.json['update_period']
        self.num_points = int(self.json['num_points'])
        self.fft_size = int(self.json['fft_size'])
        self.an_num_channels = int(self.json['an_num_channels'])
        self.num_channels = self.an_num_channels
        self.an_start_pin = int(self.json['an_start_pin'])
        self.di_num_channels = int(self.json['di_num_channels'])
        self.di_start_pin = int(self.json['di_start_pin'])
        self.samp_rate = self.json['samp_rate']
        self.center_freq = self.json['center_freq']
        self.bandwidth = self.json['bandwidth']
        self.head_var = int(self.json['head_var'])
        self.dump_file = self.json['dump_file']

        self.var_btn_run_stop = 0
        self.var_btn_save = 0

    def get_param_json(self):
        file_path = '/home/miind/Projects/gr-miindprobe/apps/variables/%s' % (self.json_file)
        with open(file_path) as json_data:
            json_param = json.load(json_data)
            print json_param
        return json_param

    def get_serial_comm(self):
        return self.serial_comm

    def set_serial_comm(self, serial_comm):
        self.serial_comm = serial_comm
        self._serial_comm_callback(self.serial_comm)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        Qt.QMetaObject.invokeMethod(self._samp_rate_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.samp_rate)))
        self.fg.app_plot_panel.set_samp_rate(self.samp_rate)
        self.fg.sdr_source_class.set_sdr_params(self, params=['samp_rate'])

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        Qt.QMetaObject.invokeMethod(self._center_freq_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.center_freq)))
        self.fg.app_plot_panel.set_center_freq(self.center_freq)
        self.fg.sdr_source_class.set_sdr_params(self, params=['center_freq'])
        
    def get_bandwidth(self):
        return self.bandwidth

    def set_bandwidth(self, bandwidth):
        self.bandwidth = bandwidth
        Qt.QMetaObject.invokeMethod(self._bandwidth_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.bandwidth)))
        self.fg.app_plot_panel.set_bandwidth(self.bandwidth)
        self.fg.sdr_source_class.set_sdr_params(self, params=['bandwidth'])
        
    def get_update_period(self):
        return self.update_period

    def set_update_period(self, update_period):
        self.update_period = update_period
        Qt.QMetaObject.invokeMethod(self._update_period_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.update_period)))
        self.fg.app_plot_panel.set_update_period(self.update_period)

    def get_num_points(self):
        return self.num_points

    def set_num_points(self, num_points):
        self.num_points = num_points
        Qt.QMetaObject.invokeMethod(self._num_points_line_edit, "setText", Qt.Q_ARG("QString", str(self.num_points)))
        self.fg.app_plot_panel.set_num_points(self.num_points)

    def get_fft_size(self):
        return self.fft_size

    def set_fft_size(self, fft_size):
        self.fft_size = fft_size
        Qt.QMetaObject.invokeMethod(self._fft_size_line_edit, "setText", Qt.Q_ARG("QString", str(self.fft_size)))
        self.fg.app_plot_panel.set_fft_size(self.fft_size)

    def get_head_var(self):
        return self.head

    def set_head_var(self, head_var):
        self.head_var = int(head_var)
        try:
            self.fg.blocks_head_tx.set_length(self.head_var)
            self.fg.blocks_head_rx.set_length(self.head_var)
        except Exception as e:
            print "Error in fxn set_head_var: " + e.message

    def get_dump_file(self):
        return self.dump_file

    def set_dump_file(self, dump_file):
        self.dump_file = str(dump_file)
        dump_file_tx = get_or_create_dump_file('tx', self.dump_file)
        dump_file_rx = get_or_create_dump_file('rx', self.dump_file)
        try:
            self.fg.blocks_file_source_tx.open(dump_file_tx, False)
            self.fg.blocks_file_source_rx.open(dump_file_rx, False)
        except Exception as e:
            print "Error in file source blocks dump_file setting: " \
                + e.message
        try:
            self.fg.blocks_file_sink_tx.open(dump_file_tx)
            self.fg.blocks_file_sink_rx.open(dump_file_rx)
        except Exception as e:
            print "Error in file sink blocks dump_file setting: " \
                + e.message

    
