# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: GPL-3.0
#
##################################################
# Flowgraphs File
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
import miindprobe

from apps.instr_app.blocks import mprobe_blocks

class continuous_fg():

    def __init__(self, var, tb, hardware, fg):
        self.tb = tb
        self.var = var
        self.hardware = hardware
        self.mprobe_blocks = mprobe_blocks(var, hardware, fg)

        self.init_blocks()

    def init_blocks(self):
        self.hw_source = self.mprobe_blocks.init_hw_source()
        self.hw_source_class = self.mprobe_blocks.hw_source_class

        self.throttle_blks = self.mprobe_blocks.init_throttle_blks()
        self.blocks_file_sink_tx = self.mprobe_blocks.init_blocks_file_sink_tx()
        self.app_plot_panel = self.mprobe_blocks.init_app_plot_panel()

    def connect_blocks(self):
        for i in range(0, self.var.num_channels):
            self.tb.connect((self.hw_source, i), (self.throttle_blks[i], 0))
            self.tb.connect((self.throttle_blks[i], 0), (self.app_plot_panel, i))
        self.tb.connect((self.throttle_blks[0], 0), (self.blocks_file_sink_tx, 0))

    def disconnect_blocks(self):
        self.tb.disconnect_all()

class head_fg(gr.hier_block2):

    def __init__(self, var, tb, hardware, fg):
        self.tb = tb
        self.var = var
        self.hardware = hardware
        self.mprobe_blocks = mprobe_blocks(var, hardware, fg)

        self.init_blocks()

    def init_blocks(self):
        self.hw_source = self.mprobe_blocks.init_hw_source()
        self.hw_source_class = self.mprobe_blocks.hw_source_class
        self.sdr_sink = self.mprobe_blocks.init_sdr_sink()
        self.sdr_sink_class = self.mprobe_blocks.sdr_sink_class

        self.hilbert_fc = self.mprobe_blocks.init_hilbert_fc()
        self.mprobe_cycle_blanker = self.mprobe_blocks.init_mprobe_cycle_blanker()
        self.analog_sig_source = self.mprobe_blocks.init_analog_sig_source()
        self.blocks_head_tx = self.mprobe_blocks.init_blocks_head_tx()
        self.blocks_head_rx = self.mprobe_blocks.init_blocks_head_rx()
        self.app_plot_panel = self.mprobe_blocks.init_app_plot_panel()

    def connect_blocks(self):
        self.tb.connect((self.analog_sig_source, 0), (self.mprobe_cycle_blanker, 0))
        self.tb.connect((self.mprobe_cycle_blanker, 0), (self.hilbert_fc, 0))
        self.tb.connect((self.hilbert_fc, 0), (self.blocks_head_tx, 0))
        self.tb.connect((self.blocks_head_tx, 0), (self.app_plot_panel, 0))
        self.tb.connect((self.blocks_head_tx, 0), (self.sdr_sink, 0))
        self.tb.connect((self.hw_source, 0), (self.blocks_head_rx, 0))
        self.tb.connect((self.blocks_head_rx, 0), (self.app_plot_panel, 1))

    def disconnect_blocks(self):
        self.tb.disconnect((self.analog_sig_source, 0), (self.mprobe_cycle_blanker, 0))
        self.tb.disconnect((self.mprobe_cycle_blanker, 0), (self.hilbert_fc, 0))
        self.tb.disconnect((self.hilbert_fc, 0), (self.blocks_head_tx, 0))
        self.tb.disconnect((self.blocks_head_tx, 0), (self.app_plot_panel, 0))
        self.tb.disconnect((self.blocks_head_tx, 0), (self.sdr_sink, 0))
        self.tb.disconnect((self.hw_source, 0), (self.blocks_head_rx, 0))
        self.tb.disconnect((self.blocks_head_rx, 0), (self.app_plot_panel, 1))


class dump_in_fg(gr.hier_block2):

    def __init__(self, var, tb, hardware, fg):
        self.tb = tb
        self.var = var
        self.hardware = hardware
        self.mprobe_blocks = mprobe_blocks(var, hardware, fg)

        self.init_blocks()

    def init_blocks(self):
        self.blocks_file_source_tx = self.mprobe_blocks.init_blocks_file_source_tx()
        self.blocks_file_source_rx = self.mprobe_blocks.init_blocks_file_source_rx()
        self.app_plot_panel = self.mprobe_blocks.init_app_plot_panel()

    def connect_blocks(self):
        self.tb.connect((self.blocks_file_source_tx, 0), (self.app_plot_panel, 0))
        self.tb.connect((self.blocks_file_source_rx, 0), (self.app_plot_panel, 1))

    def disconnect_blocks(self):
        self.tb.disconnect((self.blocks_file_source_tx, 0), (self.app_plot_panel, 0))
        self.tb.disconnect((self.blocks_file_source_rx, 0), (self.app_plot_panel, 1))


class dump_out_fg(gr.hier_block2):

    def __init__(self, var, tb, hardware, fg):
        self.tb = tb
        self.var = var
        self.hardware = hardware
        self.mprobe_blocks = mprobe_blocks(var, hardware, fg)

        self.init_blocks()

    def init_blocks(self):
        self.hw_source = self.mprobe_blocks.init_hw_source()
        self.hw_source_class = self.mprobe_blocks.hw_source_class
        self.sdr_sink = self.mprobe_blocks.init_sdr_sink()
        self.sdr_sink_class = self.mprobe_blocks.sdr_sink_class

        self.hilbert_fc = self.mprobe_blocks.init_hilbert_fc()
        self.mprobe_cycle_blanker = self.mprobe_blocks.init_mprobe_cycle_blanker()
        self.analog_sig_source = self.mprobe_blocks.init_analog_sig_source()
        self.blocks_file_sink_tx = self.mprobe_blocks.init_blocks_file_sink_tx()
        self.blocks_file_sink_rx = self.mprobe_blocks.init_blocks_file_sink_rx()
        self.app_plot_panel = self.mprobe_blocks.init_app_plot_panel()

    def connect_blocks(self):
        self.tb.connect((self.analog_sig_source, 0), (self.mprobe_cycle_blanker, 0))
        self.tb.connect((self.mprobe_cycle_blanker, 0), (self.hilbert_fc, 0))
        self.tb.connect((self.hilbert_fc, 0), (self.blocks_file_sink_tx, 0))
        self.tb.connect((self.hilbert_fc, 0), (self.app_plot_panel, 0))
        self.tb.connect((self.hilbert_fc, 0), (self.sdr_sink, 0))
        self.tb.connect((self.hw_source, 0), (self.blocks_file_sink_rx, 0))
        self.tb.connect((self.hw_source, 0), (self.app_plot_panel, 1))

    def disconnect_blocks(self):
        self.tb.disconnect((self.analog_sig_source, 0), (self.mprobe_cycle_blanker, 0))
        self.tb.disconnect((self.mprobe_cycle_blanker, 0), (self.hilbert_fc, 0))
        self.tb.disconnect((self.hilbert_fc, 0), (self.blocks_file_sink_tx, 0))
        self.tb.disconnect((self.hilbert_fc, 0), (self.app_plot_panel, 0))
        self.tb.disconnect((self.hilbert_fc, 0), (self.sdr_sink, 0))
        self.tb.disconnect((self.hw_source, 0), (self.blocks_file_sink_rx, 0))
        self.tb.disconnect((self.hw_source, 0), (self.app_plot_panel, 1))

class serial_read_fg():

    def __init__(self, var, tb, hardware, fg):
        self.tb = tb
        self.var = var
        self.hardware = hardware
        self.mprobe_blocks = mprobe_blocks(var, hardware, fg)

        self.init_blocks()

    def init_blocks(self):
        self.hw_source = self.mprobe_blocks.init_hw_source()
        self.hw_source_class = self.mprobe_blocks.hw_source_class
        self.sdr_sink = self.mprobe_blocks.init_sdr_sink()
        self.sdr_sink_class = self.mprobe_blocks.sdr_sink_class

        self.hilbert_fc = self.mprobe_blocks.init_hilbert_fc()
        self.mprobe_cycle_blanker = self.mprobe_blocks.init_mprobe_cycle_blanker()
        self.analog_sig_source = self.mprobe_blocks.init_analog_sig_source()
        self.app_plot_panel = self.mprobe_blocks.init_app_plot_panel()

    def connect_blocks(self):
        self.tb.connect((self.analog_sig_source, 0), (self.mprobe_cycle_blanker, 0))
        self.tb.connect((self.mprobe_cycle_blanker, 0), (self.hilbert_fc, 0))
        self.tb.connect((self.hilbert_fc, 0), (self.app_plot_panel, 0))
        self.tb.connect((self.hilbert_fc, 0), (self.sdr_sink, 0))
        self.tb.connect((self.hw_source, 0), (self.app_plot_panel, 1))

    def disconnect_blocks(self):
        self.tb.disconnect((self.analog_sig_source, 0), (self.mprobe_cycle_blanker, 0))
        self.tb.disconnect((self.mprobe_cycle_blanker, 0), (self.hilbert_fc, 0))
        self.tb.disconnect((self.hilbert_fc, 0), (self.app_plot_panel, 0))
        self.tb.disconnect((self.hilbert_fc, 0), (self.sdr_sink, 0))
        self.tb.disconnect((self.hw_source, 0), (self.app_plot_panel, 1))

