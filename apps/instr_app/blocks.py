import sys
import os

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
import miindprobe
import pmt

from apps.hardware.hardware import hw_source, hw_sink
from apps.instr_app.plots import mprobe_plots
from apps.instr_app.utils import get_or_create_dump_file

class mprobe_blocks():
    def __init__(self, var, hardware, blocks):
        self.var = var
        self.hardware = hardware
        self.plot = self.var.plot
        self.blocks = blocks

    def init_hw_source(self):
        # try:
        self.hw_source_class = hw_source(self.var, self.hardware)
        self.hw_source = self.hw_source_class.get_hw_source()
        # except Exception as e:
        #     print "Error instantiating source: " + e.message
        #     self.hw_source_class = self.blocks.hw_source_class
        #     self.hw_source = self.blocks.hw_source
        return self.hw_source 

    def init_hw_sink(self):
        try:
            self.hw_sink_class = hw_sink(self.var, self.hardware)
            self.hw_sink = self.hw_sink_class.get_hw_sink()
        except Exception as e:
            print "Error instantiating sink: " + e.message
            self.hw_sink_class = self.blocks.hw_sink_class
            self.hw_sink = self.blocks.hw_sink
        return self.hw_sink 

    def init_throttle_blks(self):
        self.throttle_blks = []
        for i in range(0, self.var.num_channels):
            self.throttle_blks.append(self.init_throttle_blk(i))
        return self.throttle_blks

    def init_throttle_blk(self, pin):
        try:
            throttle_blk = blocks.throttle(gr.sizeof_float*1, self.var.samp_rate,True)
        except Exception as e:
            print "Error instantiating sink: " + e.message
            throttle_blk = self.blocks.throttle_blks[pin]
        return throttle_blk

    def init_analog_sig_source(self):
        try:
            self.analog_sig_source = analog.sig_source_f(self.var.samp_rate, analog.GR_SIN_WAVE, self.var.wave_freq, 1, 0)
        except:
            self.analog_sig_source = self.blocks.analog_sig_source
        return self.analog_sig_source

    def init_app_plot_panel(self):
        try:
            self.app_plot_class = mprobe_plots(self.var, self.plot)
            self.app_plot_panel = self.app_plot_class.init_app_plot_panel()
            
        except Exception as e:
            print "Error instantiating app plot panel: " + e.message
            self.app_plot_class = self.blocks.app_plot_class
            self.app_plot_panel = self.blocks.app_plot_panel
        return self.app_plot_panel 

    def init_blocks_head_tx(self):
        try:
            self.blocks_head_tx = blocks.head(gr.sizeof_gr_complex*1, self.var.head_var)
        except:
            self.blocks_head_tx = self.blocks.blocks_head_tx
        return self.blocks_head_tx

    def init_blocks_head_rx(self):
        try:
            self.blocks_head_rx = blocks.head(gr.sizeof_gr_complex*1, self.var.head_var)
        except:
            self.blocks_head_rx = self.blocks.blocks_head_rx
        return self.blocks_head_rx

    def init_blocks_file_sink_tx(self):
        try:
            path = get_or_create_dump_file('tx', self.var.dump_file)
            print path
            self.blocks_file_sink_tx = blocks.file_sink(gr.sizeof_gr_complex, path)
            self.blocks_file_sink_tx.set_unbuffered(False)
        except Exception as e:
            print "Error init_blocks_file_sink_tx: " + str(e)
            self.blocks_file_sink_tx = self.blocks.blocks_file_sink_tx
        return self.blocks_file_sink_tx

    def init_blocks_file_sink_rx(self):
        try:
            path = get_or_create_dump_file('rx', self.var.dump_file)
            self.blocks_file_sink_rx = blocks.file_sink(gr.sizeof_gr_complex, path)
            self.blocks_file_sink_rx.set_unbuffered(False)
        except Exception as e:
            print "Error init_blocks_file_sink_rx: " + e.message
            self.blocks_file_sink_rx = self.blocks.blocks_file_sink_rx
        return self.blocks_file_sink_rx

    def init_blocks_file_source_tx(self):
        try:
            path = get_or_create_dump_file('tx', self.var.dump_file)
            print path
            self.blocks_file_source_tx = blocks.file_source(gr.sizeof_gr_complex*1, path, False)
            self.blocks_file_source_tx.set_begin_tag(pmt.PMT_NIL)
        except Exception as e:
            print "Error init_blocks_file_source_tx: " + e.message
            self.blocks_file_source_tx = self.blocks.blocks_file_source_tx
        return self.blocks_file_source_tx

    def init_blocks_file_source_rx(self):
        try:
            path = get_or_create_dump_file('rx', self.var.dump_file)
            self.blocks_file_source_rx = blocks.file_source(gr.sizeof_gr_complex*1, path, False)
            self.blocks_file_source_rx.set_begin_tag(pmt.PMT_NIL)
        except Exception as e:
            print "Error init_blocks_file_source_rx: " + e.message
            self.blocks_file_source_rx = self.blocks.blocks_file_source_rx
        return self.blocks_file_source_rx

    
