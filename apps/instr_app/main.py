#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: GPL-3.0
#
##################################################
# GNU Radio Python Flow Graph
# Title: mprobe
# Author: Daryl Pongcol
# Copyright: berdware
# Generated: Tue Sep  4 21:58:15 2018
# GNU Radio version: 3.7.12.0
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

import os
import sys
import time
import argparse
import qdarkstyle
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import blocks
from gnuradio import gr
from gnuradio import qtgui

from apps.widgets.topbar import mprobe_topbar
from apps.widgets.sidebar import mprobe_sidebar
from apps.instr_app.variables import mprobe_variables
from apps.instr_app.layouts import mprobe_layouts
from apps.instr_app.flowgraphs import continuous_fg, head_fg, dump_in_fg, dump_out_fg

class instr_app(mprobe_variables, mprobe_layouts,
                mprobe_topbar, mprobe_sidebar,
                Qt.QWidget):
    
    def __init__(self, args):
        self.tb = gr.top_block()
        Qt.QWidget.__init__(self)
        self.setWindowTitle("mprobe")
        # qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon('resources/mprobe_instr.png'))
            self.setStyleSheetFromFile('/home/ergwd/prefix/default/share/gnuradio/themes/dark.qss')
        except Exception as e:
            print e.message
        
        # Main variables
        self.args = args
        self.flowgraph = self.args.flowgraph
        self.hardware = self.args.hardware
        self.plot = self.args.plot
        self.is_running = False
        self.fg = None
        
        mprobe_variables.__init__(self, json_file=self.args.variables_file)
        self.set_fg()
        mprobe_layouts.__init__(self)

        if self.args.default_running:
            self.run_system()

    def set_fg(self):
        if self.flowgraph == "continuous":
            print "switching to continuous flowgraph"
            self.lock_tb()
            self.disconnect_fg()
            self.fg = continuous_fg(self, self.tb, self.hardware, self.fg) 
            print "using the continuous flowgraph now"
            self.connect_fg()
            self.unlock_tb()
        elif self.flowgraph == "head":
            print "switching to head flowgraph"
            self.lock_tb()
            self.disconnect_fg()
            self.fg = head_fg(self, self.tb, self.hardware, self.fg) 
            print "using the head flowgraph with head blocks '%s' & '%s'" \
                % (str(self.fg.blocks_head_0), str(self.fg.blocks_head_1))
            self.connect_fg()
            self.unlock_tb()
        elif self.flowgraph == "data_dump_in":
            print "switching to data_dump_in flowgraph"
            self.lock_tb()
            self.disconnect_fg()
            self.fg = dump_in_fg(self, self.tb, self.hardware, self.fg) 
            print "using the data_dump_in flowgraph with file src blocks '%s' & '%s'" \
                % (str(self.fg.blocks_file_source_0), str(self.fg.blocks_file_source_1))
            self.connect_fg()
            self.unlock_tb()
        elif self.flowgraph == "data_dump_out":
            print "switching to data_dump_out flowgraph"
            self.lock_tb()
            self.disconnect_fg()
            self.fg = dump_out_fg(self, self.tb, self.hardware, self.fg) 
            print "using the data_dump_out flowgraph with file snk blocks '%s' & '%s'" \
                % (str(self.fg.blocks_file_sink_0), str(self.fg.blocks_file_sink_1))
            self.connect_fg()
            self.unlock_tb()
        else:
            print "switching to continuous flowgraph"
            self.lock_tb()
            self.disconnect_fg()
            self.fg = continuous_fg(self, self.tb, self.hardware, self.fg) 
            print "using the continuous flowgraph now"
            self.connect_fg()
            self.unlock_tb()
    
    def lock_tb(self):
        self.tb.lock()
        print "tb is locked"

    def unlock_tb(self):
        self.tb.unlock()
        self.tb.wait()
        print "tb is unlocked"

    def connect_fg(self):
        try:
            self.fg.connect_blocks()
        except Exception as e:
            print "Error connecting all blocks: " + e.message

    def disconnect_fg(self):
        try:
            if self.fg:
                self.fg.disconnect_blocks()
        except Exception as e:
            print "Error disconnecting all blocks: " + e.message

    def get_hardware(self):
        return self.hardware

    def set_hardware(self, hardware):
        self.hardware = hardware
        self.set_fg()

    def get_flowgraph(self):
        return self.flowgraph

    def set_flowgraph(self, flowgraph):
        self.flowgraph = flowgraph
        self.set_fg()
        self._flowgraph_callback(self.flowgraph)

    def run_system(self):
        self.tb.start()
        self.is_running = True

    def stop_system(self):
        self.tb.stop()
        self.tb.wait()
        self.is_running = False

    def setStyleSheetFromFile(self, file_path):
        self.tb.setStyleSheetFromFile(file_path)

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "ascan_plutosdr_txrx")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    
def options():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument ("-fg", "--flowgraph", type=str, default='continuous',
                        help="set the flowgraph [default=%default]", metavar="FREQ")
    parser.add_argument ("-hw", "--hardware", type=str, default='arduino_uno',
                        help="set the hardware [default=%default]")
    parser.add_argument ("-p", "--plot", type=str, default='timefreq',
                        help="set the plot panel [default=%default]")
    parser.add_argument ("-th", "--theme", type=str, default='plain',
                        help="set the app style theme [default=%default]")
    parser.add_argument ("-vf", "--variables-file", default="default.json",
                        help="set the variables json file to use", metavar="FILE")
    parser.add_argument ("-dr", "--default-running", action='store_true',
                        help="set the is_running to True by default")
    return parser

def main(app=instr_app, options=options):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    # instantiate args and app
    parser = options()
    args = parser.parse_args() 
    app = app(args)

    # set theme
    if args.theme == 'dark':
        app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt())
    else:
        pass

    # show
    app.show()

    def quitting():
        app.tb.stop()
        app.tb.wait()
        parser.exit(0)
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
