import os
import json
import argparse
import serial as ser
import numpy as np

class Serial(object):
    def __init__(self, serial_port, baud_rate,
                 num_channels, output_msg_type):
        self.output_msg_type = output_msg_type
        self.num_channels = num_channels
        self.buffer_size = 0

        try:
            self.serial = ser.Serial(serial_port, baud_rate)
        except Exception as e:
            self.serial = ser.Serial(serial_port, baud_rate)
        
        if self.output_msg_type == "text":
            print "Starting serial communication."
            print self.serial   

        self.init_outfiles_and_buffers()

    def init_outfiles_and_buffers(self):
        self.outfiles = []
        self.outbuffers = []
        for i in range(0, self.num_channels):
            path = "%s/serialfiles/analog_read_ch%d.dat" %(os.getcwd(), i + 1)
            self.outfiles.append(open(path, "w+"))
            self.outbuffers.append([])

    def close_outfiles(self):
        self.outfiles = []
        for i in range(0, self.num_channels):
            self.outfiles[i].close()

    def read_pins(self):    
        data = []
        for i in range(0, self.num_channels):
            try:
                data.append(float(self.serial.read()))
            except ValueError:
                data.append(0)
        # print data
        
        return data

    def read_line(self):    
        data = self.serial.readline()
        data = data.split(" ")
        # data[self.num_channels-1] = data[self.num_channels-1].replace("\r\n", "")
        # print data
        
        return data

    def append_to_outbuffers(self, data_read):
        for i in range(0, self.num_channels):
            print "outbuffer[%d][%d] \t = \t %d" %(i,  self.buffer_size, data_read[i])
            self.outbuffers[i].append(data_read[i])
            self.buffer_size += 1

    def append_bin_to_outfiles(self):
        for i in range(0, self.num_channels):
            data_out = np.append(self.outbuffers[i], dtype=np.float32)
            data_out.tofile(self.outfiles[i])
            
    def write_to_outfiles(self):
        for i in range(0, self.num_channels):
            self.outfiles[i].write(str(self.outbuffers[i]))
            self.outfiles[i].flush()
        self.buffer_size = 0

    def write_bin_to_outfiles(self):
        for i in range(0, self.num_channels):
            data_out = np.array(self.outbuffers[i], dtype=np.float32)
            data_out.tofile(self.outfiles[i])
        # self.buffer_size = 0

    def flush_outfiles(self):
        for i in range(0, self.num_channels):
            self.outfiles[i].flush()
        self.buffer_size = 0

    def quit(self):
        self.serial = None
        self.close_outfiles()
        if self.output_msg_type == "text":
            print "Serial reading is now terminated."

    def __str__(self):
        return "{}".format(self.serial)

def options():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument ("-br", "--baud-rate", type=int, default=115200,
                        help="set the baudrate for serial comms [default=%default]", metavar="FREQ")
    parser.add_argument ("-sp", "--serial-port", type=str, default='/dev/ttyUSB0',
                        help="set the serial port for serial comms [default=%default]")
    parser.add_argument ("-nc", "--num-channels", type=int, default=4,
                        help="set the number of analog channels to read [default=%default]")
    return parser

def main(args):
    
    serial = Serial(args.serial_port, args.baud_rate, args.num_channels, "text")
    try:
        while True:
            analog_reads = serial.read_line()
            data_read = []
            for i in range(0, args.num_channels):
                try:
                    analog_pin_val = float(analog_reads[i])
                except:
                    analog_pin_val = 0
                data_read.append(analog_pin_val)
            
            serial.append_to_outbuffers(data_read)
            serial.write_bin_to_outfiles()
            if serial.buffer_size == 8000:
                serial.flush_outfiles()
    except KeyboardInterrupt:
        print "\nKeyboard interrupted."
        serial.quit()

    

if __name__ == '__main__':
    parser = options()
    args = parser.parse_args() 
    main(args)
