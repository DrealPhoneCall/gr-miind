import sys
import os

# grc-generated hier_block
from qt_multichannel_timefreq_sink import qt_multichannel_timefreq_sink
from qt_multichannel_tabbed_timefreq_sink import qt_multichannel_tabbed_timefreq_sink
from qt_multichannel_gui_sink import qt_multichannel_gui_sink

"""
    Main class for the plotter blocks of the apps.
    
    Legend:        
        QGS - Qt GUI Sink
        QTS - Qt Time Sink
        QFS - Qt Frequency Sink
        QRS - Qt Time Raster Sink
        QWS - Qt Freq Waterfall Sink

        TX - Transmission
        RX - Reception
    
"""

class mprobe_plots():
    def __init__(self, var, plot):
        self.var = var
        self.plot = plot

    def init_app_plot_panel(self):
        if self.plot in ['timefreq', 'time-freq']:
            return self.init_time_freq_plot_panel()
        elif self.plot in ['tabbed-timefreq', 'tabbed-time-freq']:
            return self.init_tabbed_time_freq_plot_panel()
        elif self.plot in ['gui', 'gui-sink']:
            return self.init_gui_plot_panel()
        else:
            pass

    def init_time_freq_plot_panel(self):
        try:
            self.app_plot_panel = qt_multichannel_timefreq_sink(
                samp_rate=self.var.samp_rate,
                bandwidth=self.var.bandwidth,
                center_freq=self.var.center_freq,
                fft_size=self.var.fft_size,
                num_points=self.var.num_points,
                update_period=self.var.update_period
            )
        except Exception as e:
            print "Error instantiating plot sink: " + e.message
            self.app_plot_panel = self.blocks.app_plot_panel
        return self.app_plot_panel 

    def init_tabbed_time_freq_plot_panel(self):
        try:
            self.app_plot_panel = qt_multichannel_tabbed_timefreq_sink(
                samp_rate=self.var.samp_rate,
                bandwidth=self.var.bandwidth,
                center_freq=self.var.center_freq,
                fft_size=self.var.fft_size,
                num_points=self.var.num_points,
                update_period=self.var.update_period
            )
        except Exception as e:
            print "Error instantiating plot sink: " + e.message
            self.app_plot_panel = self.blocks.app_plot_panel
        return self.app_plot_panel 

    def init_gui_plot_panel(self):
        try:
            self.app_plot_panel = qt_multichannel_gui_sink(
                samp_rate=self.var.samp_rate,
                bandwidth=self.var.bandwidth,
                center_freq=self.var.center_freq,
                fft_size=self.var.fft_size,
                num_points=self.var.num_points,
                update_period=self.var.update_period
            )
        except Exception as e:
            print "Error instantiating plot sink: " + e.message
            self.app_plot_panel = self.blocks.app_plot_panel
        return self.app_plot_panel
