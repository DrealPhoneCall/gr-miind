import json

from PyQt4 import Qt
from gnuradio import qtgui
from PyQt4.QtCore import QObject, pyqtSlot

from apps.widgets.topbar import mprobe_topbar
from apps.widgets.sidebar import mprobe_sidebar

class mprobe_layouts(Qt.QWidget):
    def __init__(self, flowgraph="continuous run"):
        Qt.QWidget.__init__(self)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)
        self.setLayout(self.top_layout)

        self.settings = Qt.QSettings("GNU Radio", "miindprobe")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        self.topbar_layout_json = self.get_layout_json('topbar.layout.json')
        self.plots_layout_json = self.get_layout_json('plots.layout.json')
        self.sidebar_layout_json = self.get_layout_json('sidebar.layout.json')

        mprobe_sidebar.__init__(self)
        # mprobe_topbar.__init__(self)
        
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self.fg.app_plot_panel, 
                                    self.plots_layout_json['overall'],
                                    self.plots_layout_json['ascan_plot'])

    def set_widget_grid_layout(self, grid_layout, widget, overall, grid):
        x = overall['x'] + grid['x']
        y = overall['y'] + grid['y']
        grid_layout.addWidget(widget, y, x, grid['h'], grid['w'])
        for r in range(y, y + grid['h']):
            grid_layout.setRowStretch(r, 1)
        for c in range(x, x + grid['w']):
            grid_layout.setColumnStretch(c, 1)

    def get_layout_json(self, layout_json_file):
        file_path = '/home/ergwd/Projects/gr-miindprobe/apps/layouts/%s' % (layout_json_file)
        with open(file_path) as json_data:
            json_param = json.load(json_data)
        return json_param

    