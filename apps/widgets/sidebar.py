# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: GPL-3.0
#
##################################################
# PyQt Widget
# Title: Top Bar Widget
# Author: Daryl Pongcol
# Copyright: DOST PCIEERD
# Description: Top bar widget to be used for 
#              multiple apps
#
##################################################

import os

from PyQt4 import Qt
from gnuradio import qtgui
from PyQt4.QtCore import QObject, pyqtSlot

from gnuradio import eng_notation
from gnuradio.eng_option import eng_option

class mprobe_sidebar():

    def __init__(self):
        print "top_grid_layout" + str(self.top_grid_layout)

        self._serial_comm_options = ('serial', 'firmata' )
        self._serial_comm_labels = ('Serial', 'Firmata' )
        self._serial_comm_group_box = Qt.QGroupBox('Serial Comm')
        self._serial_comm_box = Qt.QVBoxLayout()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._serial_comm_button_group = variable_chooser_button_group()
        self._serial_comm_group_box.setLayout(self._serial_comm_box)
        for i, label in enumerate(self._serial_comm_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._serial_comm_box.addWidget(radio_button)
        	self._serial_comm_button_group.addButton(radio_button, i)
        self._serial_comm_callback = lambda i: Qt.QMetaObject.invokeMethod(self._serial_comm_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._serial_comm_options.index(i)))
        self._serial_comm_callback(self.serial_comm)
        self._serial_comm_button_group.buttonClicked[int].connect(
        	lambda i: self.set_serial_comm(self._serial_comm_options[i]))
        
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self._serial_comm_group_box, 
                                    self.sidebar_layout_json['overall'],
                                    self.sidebar_layout_json['serial_comm'])

        self._hardware_options = ('arduino_uno', 'arduino_nano', 'fpga')
        self._hardware_labels = ('Uno', 'Nano', 'FPGA' )
        self._hardware_tool_bar = Qt.QToolBar(self)
        self._hardware_tool_bar.addWidget(Qt.QLabel('Hardware '))
        self._hardware_combo_box = Qt.QComboBox()
        self._hardware_tool_bar.addWidget(self._hardware_combo_box)
        for label in self._hardware_labels: self._hardware_combo_box.addItem(label)
        self._hardware_callback = lambda i: Qt.QMetaObject.invokeMethod(self._hardware_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._hardware_options.index(i)))
        self._hardware_callback(self.hardware)
        self._hardware_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_hardware(self._hardware_options[i]))
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self._hardware_tool_bar, 
                                    self.sidebar_layout_json['overall'],
                                    self.sidebar_layout_json['hardware'])

        # TODO: get serial ports
        self._serial_port_options = ('/dev/ttyUSB0', '/dev/ttyUSB1', '/dev/ttyUSB2', '/dev/ttyACM0')
        self._serial_port_labels = ('/dev/ttyUSB0', '/dev/ttyUSB1', '/dev/ttyUSB2', '/dev/ttyACM0')
        self._serial_port_tool_bar = Qt.QToolBar(self)
        self._serial_port_tool_bar.addWidget(Qt.QLabel('Port '))
        self._serial_port_combo_box = Qt.QComboBox()
        self._serial_port_tool_bar.addWidget(self._serial_port_combo_box)
        for label in self._serial_port_labels: self._serial_port_combo_box.addItem(label)
        self._serial_port_callback = lambda i: Qt.QMetaObject.invokeMethod(self._serial_port_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._serial_port_options.index(i)))
        self._serial_port_callback(self.serial_port)
        self._serial_port_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_serial_port(self._serial_port_options[i]))
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self._serial_port_tool_bar, 
                                    self.sidebar_layout_json['overall'],
                                    self.sidebar_layout_json['serial_port'])

        self._baud_rate_options = ('9600', '115200')
        self._baud_rate_labels = ('9600', '115200')
        self._baud_rate_tool_bar = Qt.QToolBar(self)
        self._baud_rate_tool_bar.addWidget(Qt.QLabel('Baud Rate '))
        self._baud_rate_combo_box = Qt.QComboBox()
        self._baud_rate_tool_bar.addWidget(self._baud_rate_combo_box)
        for label in self._baud_rate_labels: self._baud_rate_combo_box.addItem(label)
        self._baud_rate_callback = lambda i: Qt.QMetaObject.invokeMethod(self._baud_rate_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._baud_rate_options.index(i)))
        self._baud_rate_callback(self.baud_rate)
        self._baud_rate_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_baud_rate(self._baud_rate_options[i]))
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self._baud_rate_tool_bar, 
                                    self.sidebar_layout_json['overall'],
                                    self.sidebar_layout_json['baud_rate'])

        self.ad_tab_widget = Qt.QTabWidget()
        self.ad_tab_widget_widget_0 = Qt.QWidget()
        self.ad_tab_widget_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.ad_tab_widget_widget_0)
        self.ad_tab_widget_grid_layout_0 = Qt.QGridLayout()
        self.ad_tab_widget_layout_0.addLayout(self.ad_tab_widget_grid_layout_0)
        self.ad_tab_widget.addTab(self.ad_tab_widget_widget_0, 'Analog')
        self.ad_tab_widget_widget_1 = Qt.QWidget()
        self.ad_tab_widget_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.ad_tab_widget_widget_1)
        self.ad_tab_widget_grid_layout_1 = Qt.QGridLayout()
        self.ad_tab_widget_layout_1.addLayout(self.ad_tab_widget_grid_layout_1)
        self.ad_tab_widget.addTab(self.ad_tab_widget_widget_1, 'Digital')
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self.ad_tab_widget, 
                                    self.sidebar_layout_json['overall'],
                                    self.sidebar_layout_json['ad_tab_widget']['overall'])

        self._an_num_channels_tool_bar = Qt.QToolBar(self)
        self._an_num_channels_tool_bar.addWidget(Qt.QLabel('# of Channels '))
        self._an_num_channels_line_edit = Qt.QLineEdit(str(self.an_num_channels))
        self._an_num_channels_tool_bar.addWidget(self._an_num_channels_line_edit)
        self._an_num_channels_line_edit.returnPressed.connect(
        	lambda: self.set_an_num_channels(int(str(self._an_num_channels_line_edit.text().toAscii()))))
        self.set_widget_grid_layout(self.ad_tab_widget_grid_layout_0, 
                                    self._an_num_channels_tool_bar, 
                                    self.sidebar_layout_json['ad_tab_widget']['grid_layout_0']['overall'],
                                    self.sidebar_layout_json['ad_tab_widget']['grid_layout_0']['an_num_channels'])

        self._an_start_pin_tool_bar = Qt.QToolBar(self)
        self._an_start_pin_tool_bar.addWidget(Qt.QLabel('Start Pin '))
        self._an_start_pin_line_edit = Qt.QLineEdit(str(self.an_start_pin))
        self._an_start_pin_tool_bar.addWidget(self._an_start_pin_line_edit)
        self._an_start_pin_line_edit.returnPressed.connect(
        	lambda: self.set_an_start_pin(int(str(self._an_start_pin_line_edit.text().toAscii()))))
        self.set_widget_grid_layout(self.ad_tab_widget_grid_layout_0, 
                                    self._an_start_pin_tool_bar, 
                                    self.sidebar_layout_json['ad_tab_widget']['grid_layout_0']['overall'],
                                    self.sidebar_layout_json['ad_tab_widget']['grid_layout_0']['an_start_pin'])

        self._di_num_channels_tool_bar = Qt.QToolBar(self)
        self._di_num_channels_tool_bar.addWidget(Qt.QLabel('# of Channels '))
        self._di_num_channels_line_edit = Qt.QLineEdit(str(self.di_num_channels))
        self._di_num_channels_tool_bar.addWidget(self._di_num_channels_line_edit)
        self._di_num_channels_line_edit.returnPressed.connect(
        	lambda: self.set_di_num_channels(int(str(self._di_num_channels_line_edit.text().toAscii()))))
        self.set_widget_grid_layout(self.ad_tab_widget_grid_layout_1, 
                                    self._di_num_channels_tool_bar, 
                                    self.sidebar_layout_json['ad_tab_widget']['grid_layout_1']['overall'],
                                    self.sidebar_layout_json['ad_tab_widget']['grid_layout_1']['di_num_channels'])

        self._di_start_pin_tool_bar = Qt.QToolBar(self)
        self._di_start_pin_tool_bar.addWidget(Qt.QLabel('Start Pin '))
        self._di_start_pin_line_edit = Qt.QLineEdit(str(self.di_start_pin))
        self._di_start_pin_tool_bar.addWidget(self._di_start_pin_line_edit)
        self._di_start_pin_line_edit.returnPressed.connect(
        	lambda: self.set_di_start_pin(int(str(self._di_start_pin_line_edit.text().toAscii()))))
        self.set_widget_grid_layout(self.ad_tab_widget_grid_layout_1, 
                                    self._di_start_pin_tool_bar, 
                                    self.sidebar_layout_json['ad_tab_widget']['grid_layout_1']['overall'],
                                    self.sidebar_layout_json['ad_tab_widget']['grid_layout_1']['di_start_pin'])

        self._samp_rate_tool_bar = Qt.QToolBar(self)
        self._samp_rate_tool_bar.addWidget(Qt.QLabel('Samp Rate '))
        self._samp_rate_line_edit = Qt.QLineEdit(str(self.samp_rate))
        self._samp_rate_tool_bar.addWidget(self._samp_rate_line_edit)
        self._samp_rate_line_edit.returnPressed.connect(
        	lambda: self.set_samp_rate(eng_notation.str_to_num(str(self._samp_rate_line_edit.text().toAscii()))))
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self._samp_rate_tool_bar, 
                                    self.sidebar_layout_json['overall'],
                                    self.sidebar_layout_json['samp_rate'])

        self._bandwidth_tool_bar = Qt.QToolBar(self)
        self._bandwidth_tool_bar.addWidget(Qt.QLabel('Bandwidth '))
        self._bandwidth_line_edit = Qt.QLineEdit(str(self.bandwidth))
        self._bandwidth_tool_bar.addWidget(self._bandwidth_line_edit)
        self._bandwidth_line_edit.returnPressed.connect(
        	lambda: self.set_bandwidth(eng_notation.str_to_num(str(self._bandwidth_line_edit.text().toAscii()))))
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self._bandwidth_tool_bar, 
                                    self.sidebar_layout_json['overall'],
                                    self.sidebar_layout_json['bandwidth'])

        self._center_freq_tool_bar = Qt.QToolBar(self)
        self._center_freq_tool_bar.addWidget(Qt.QLabel('Center Freq '))
        self._center_freq_line_edit = Qt.QLineEdit(str(self.center_freq))
        self._center_freq_tool_bar.addWidget(self._center_freq_line_edit)
        self._center_freq_line_edit.returnPressed.connect(
        	lambda: self.set_center_freq(eng_notation.str_to_num(str(self._center_freq_line_edit.text().toAscii()))))
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self._center_freq_tool_bar, 
                                    self.sidebar_layout_json['overall'],
                                    self.sidebar_layout_json['center_freq'])

        self._btn_run_stop_push_button = Qt.QPushButton('Run / Stop')
        self._btn_run_stop_choices = {'Pressed': 1, 'Released': 0}
        self._btn_run_stop_push_button.pressed.connect(self.btn_run_stop_pressed)
        self._btn_run_stop_push_button.released.connect(self.btn_run_stop_released)
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self._btn_run_stop_push_button, 
                                    self.sidebar_layout_json['overall'],
                                    self.sidebar_layout_json['btn_run_stop'])

        self._btn_save_push_button = Qt.QPushButton('Save')
        self._btn_save_choices = {'Pressed': 1, 'Released': 0}
        self._btn_save_push_button.pressed.connect(self.btn_save_pressed)
        self._btn_save_push_button.released.connect(self.btn_save_released)
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self._btn_save_push_button, 
                                    self.sidebar_layout_json['overall'],
                                    self.sidebar_layout_json['btn_save'])

    def get_btn_run_stop(self):
        return self.btn_run_stop

    def btn_run_stop_pressed(self):
        print "run button is pressed"

    def btn_run_stop_released(self):
        print "run button is released"
        if self.is_running:
            print "is_running = True --> stopping system"
            self.stop_system()
        else:
            print "is_running = False --> starting system"
            self.run_system()

    def get_btn_save(self):
        return self.btn_save

    def btn_save_pressed(self):
        print "save button is pressed"

    def btn_save_released(self):
        print "save button is released"
        self.save_system()