# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: GPL-3.0
#
##################################################
# PyQt Widget
# Title: Top Bar Widget
# Author: Daryl Pongcol
# Copyright: DOST PCIEERD
# Description: Top bar widget to be used for 
#              multiple apps
#
##################################################

import os

from PyQt4 import Qt
from gnuradio import qtgui


class mprobe_topbar():

    def __init__(self):
        print "top_grid_layout" + str(self.top_grid_layout)

        self.logo = Qt.QLabel('miindProbe | Instrumentation')
        self.logo.setPixmap(Qt.QPixmap(os.getcwd() + "/apps/resources/mprobe_instr.png"))
        self.logo.setGeometry(5, 10, 5, 10)
        self.set_widget_grid_layout(self.top_grid_layout, 
                                    self.logo, 
                                    self.topbar_layout_json['overall'],
                                    self.topbar_layout_json['logo'])

        # _var_btn_wfall_push_button = Qt.QPushButton('Waterfall')
        # self._var_btn_wfall_choices = {'Pressed': 1, 'Released': 0}
        # _var_btn_wfall_push_button.pressed.connect(lambda: self.set_var_btn_wfall(self._var_btn_wfall_choices['Pressed']))
        # _var_btn_wfall_push_button.released.connect(lambda: self.set_var_btn_wfall(self._var_btn_wfall_choices['Released']))
        # self.set_widget_grid_layout(self.top_grid_layout, 
        #                             _var_btn_wfall_push_button, 
        #                             self.topbar_layout_json['overall'],
        #                             self.topbar_layout_json['btn_wfall'])

        # _var_btn_fft_push_button = Qt.QPushButton('Spectrum')
        # self._var_btn_fft_choices = {'Pressed': 1, 'Released': 0}
        # _var_btn_fft_push_button.pressed.connect(lambda: self.set_var_btn_fft(self._var_btn_fft_choices['Pressed']))
        # _var_btn_fft_push_button.released.connect(lambda: self.set_var_btn_fft(self._var_btn_fft_choices['Released']))
        # self.set_widget_grid_layout(self.top_grid_layout, 
        #                             _var_btn_fft_push_button, 
        #                             self.topbar_layout_json['overall'],
        #                             self.topbar_layout_json['btn_fft'])

        # var_btn_raster_push_button = Qt.QPushButton('Raster')
        # self.var_btn_raster_choices = {'Pressed': 1, 'Released': 0}
        # var_btn_raster_push_button.pressed.connect(lambda: self.var_btn_raster(self.var_btn_raster_choices['Pressed']))
        # var_btn_raster_push_button.released.connect(lambda: self.var_btn_raster(self.var_btn_raster_choices['Released']))
        # self.set_widget_grid_layout(self.top_grid_layout, 
        #                             var_btn_raster_push_button, 
        #                             self.topbar_layout_json['overall'],
        #                             self.topbar_layout_json['btn_raster'])

        # _var_btn_const_push_button = Qt.QPushButton('Constellation')
        # self._var_btn_const_choices = {'Pressed': 1, 'Released': 0}
        # _var_btn_const_push_button.pressed.connect(lambda: self.set_var_btn_const(self._var_btn_const_choices['Pressed']))
        # _var_btn_const_push_button.released.connect(lambda: self.set_var_btn_const(self._var_btn_const_choices['Released']))
        # self.set_widget_grid_layout(self.top_grid_layout, 
        #                             _var_btn_const_push_button, 
        #                             self.topbar_layout_json['overall'],
        #                             self.topbar_layout_json['btn_const'])

    def get_var_btn_const(self):
        return self.var_btn_const

    def set_var_btn_const(self, var_btn_const):
        self.var_btn_const = var_btn_const

    def var_btn_raster(self):
        return self.var_btn_raster

    def var_btn_raster(self, var_btn_raster):
        self.var_btn_raster = var_btn_raster

    def get_var_btn_fft(self):
        return self.var_btn_fft

    def set_var_btn_fft(self, var_btn_fft):
        self.var_btn_fft = var_btn_fft

    def get_var_btn_wfall(self):
        return self.var_btn_wfall

    def set_var_btn_wfall(self, var_btn_wfall):
        self.var_btn_wfall = var_btn_wfall
