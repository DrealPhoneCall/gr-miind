# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'topbar.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_TopBar(object):
    def setupUi(self, TopBar):
        TopBar.setObjectName(_fromUtf8("TopBar"))
        TopBar.resize(702, 31)
        self.horizontalLayoutWidget = QtGui.QWidget(TopBar)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 701, 31))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.hztal_layout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.hztal_layout.setObjectName(_fromUtf8("hztal_layout"))
        self.lbl_logo = QtGui.QLabel(self.horizontalLayoutWidget)
        self.lbl_logo.setText(_fromUtf8(""))
        self.lbl_logo.setPixmap(QtGui.QPixmap(_fromUtf8("../resources/mprobe_ascan.png")))
        self.lbl_logo.setScaledContents(True)
        self.lbl_logo.setObjectName(_fromUtf8("lbl_logo"))
        self.hztal_layout.addWidget(self.lbl_logo)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.hztal_layout.addItem(spacerItem)
        self.btn_fft = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.btn_fft.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(50)
        sizePolicy.setHeightForWidth(self.btn_fft.sizePolicy().hasHeightForWidth())
        self.btn_fft.setSizePolicy(sizePolicy)
        self.btn_fft.setObjectName(_fromUtf8("btn_fft"))
        self.hztal_layout.addWidget(self.btn_fft)
        self.btn_wfall = QtGui.QPushButton(self.horizontalLayoutWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_wfall.sizePolicy().hasHeightForWidth())
        self.btn_wfall.setSizePolicy(sizePolicy)
        self.btn_wfall.setObjectName(_fromUtf8("btn_wfall"))
        self.hztal_layout.addWidget(self.btn_wfall)
        self.btn_raster = QtGui.QPushButton(self.horizontalLayoutWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_raster.sizePolicy().hasHeightForWidth())
        self.btn_raster.setSizePolicy(sizePolicy)
        self.btn_raster.setObjectName(_fromUtf8("btn_raster"))
        self.hztal_layout.addWidget(self.btn_raster)
        self.btn_const = QtGui.QPushButton(self.horizontalLayoutWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_const.sizePolicy().hasHeightForWidth())
        self.btn_const.setSizePolicy(sizePolicy)
        self.btn_const.setObjectName(_fromUtf8("btn_const"))
        self.hztal_layout.addWidget(self.btn_const)
        self.line = QtGui.QFrame(self.horizontalLayoutWidget)
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.hztal_layout.addWidget(self.line)
        self.btn_txrx = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.btn_txrx.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(50)
        sizePolicy.setHeightForWidth(self.btn_txrx.sizePolicy().hasHeightForWidth())
        self.btn_txrx.setSizePolicy(sizePolicy)
        self.btn_txrx.setMaximumSize(QtCore.QSize(16777215, 100))
        self.btn_txrx.setObjectName(_fromUtf8("btn_txrx"))
        self.hztal_layout.addWidget(self.btn_txrx)

        self.retranslateUi(TopBar)
        QtCore.QMetaObject.connectSlotsByName(TopBar)

    def retranslateUi(self, TopBar):
        TopBar.setWindowTitle(_translate("TopBar", "Form", None))
        self.btn_fft.setText(_translate("TopBar", "FFT", None))
        self.btn_wfall.setText(_translate("TopBar", "WFall", None))
        self.btn_raster.setText(_translate("TopBar", "Raster", None))
        self.btn_const.setText(_translate("TopBar", "Const", None))
        self.btn_txrx.setText(_translate("TopBar", "Tx Rx", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    TopBar = QtGui.QWidget()
    ui = Ui_TopBar()
    ui.setupUi(TopBar)
    TopBar.show()
    sys.exit(app.exec_())

