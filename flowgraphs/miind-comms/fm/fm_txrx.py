#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: GPL-3.0
#
##################################################
# GNU Radio Python Flow Graph
# Title: Fm Txrx
# Generated: Wed Oct 31 15:20:14 2018
# GNU Radio version: 3.7.12.0
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from gnuradio import analog
from gnuradio import audio
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import wxgui
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from gnuradio.wxgui import fftsink2
from gnuradio.wxgui import forms
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import osmosdr
import time
import wx


class fm_txrx(grc_wxgui.top_block_gui):

    def __init__(self):
        grc_wxgui.top_block_gui.__init__(self, title="Fm Txrx")

        ##################################################
        # Variables
        ##################################################
        self.tx_freq_var = tx_freq_var = 93e6
        self.tx_freq = tx_freq = 87.7e6
        self.samp_rate = samp_rate = 10e6
        self.audio_freq = audio_freq = 48e3

        ##################################################
        # Blocks
        ##################################################
        self.wxgui_fftsink2_0 = fftsink2.fft_sink_c(
        	self.GetWin(),
        	baseband_freq=0,
        	y_per_div=10,
        	y_divs=10,
        	ref_level=0,
        	ref_scale=2.0,
        	sample_rate=samp_rate,
        	fft_size=1024,
        	fft_rate=15,
        	average=False,
        	avg_alpha=None,
        	title='FFT Plot',
        	peak_hold=False,
        )
        self.Add(self.wxgui_fftsink2_0.win)
        _tx_freq_sizer = wx.BoxSizer(wx.VERTICAL)
        self._tx_freq_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_tx_freq_sizer,
        	value=self.tx_freq,
        	callback=self.set_tx_freq,
        	label='tx_freq',
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._tx_freq_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_tx_freq_sizer,
        	value=self.tx_freq,
        	callback=self.set_tx_freq,
        	minimum=87.7e6,
        	maximum=107.7e6,
        	num_steps=int(1000),
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_tx_freq_sizer)
        self.rational_resampler_xxx_1 = filter.rational_resampler_ccc(
                interpolation=90,
                decimation=4,
                taps=None,
                fractional_bw=None,
        )
        self.osmosdr_sink_0 = osmosdr.sink( args="numchan=" + str(1) + " " + '' )
        self.osmosdr_sink_0.set_sample_rate(samp_rate)
        self.osmosdr_sink_0.set_center_freq(tx_freq_var, 0)
        self.osmosdr_sink_0.set_freq_corr(0, 0)
        self.osmosdr_sink_0.set_gain(30, 0)
        self.osmosdr_sink_0.set_if_gain(20, 0)
        self.osmosdr_sink_0.set_bb_gain(20, 0)
        self.osmosdr_sink_0.set_antenna('', 0)
        self.osmosdr_sink_0.set_bandwidth(0, 0)

        self.blocks_wavfile_source_0 = blocks.wavfile_source('/home/ergwd/Projects/gr-miind/flowgraphs/miind-radio/sample-audio/franco-a-beautiful-diversion.wav', True)
        self.audio_sink_0 = audio.sink(int(audio_freq), '', True)
        self.analog_wfm_tx_0 = analog.wfm_tx(
        	audio_rate=int(4*audio_freq),
        	quad_rate=int(4*audio_freq),
        	tau=25e-6,
        	max_dev=75e3,
        	fh=-1.0,
        )



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_wfm_tx_0, 0), (self.rational_resampler_xxx_1, 0))
        self.connect((self.blocks_wavfile_source_0, 0), (self.analog_wfm_tx_0, 0))
        self.connect((self.blocks_wavfile_source_0, 0), (self.audio_sink_0, 0))
        self.connect((self.rational_resampler_xxx_1, 0), (self.osmosdr_sink_0, 0))
        self.connect((self.rational_resampler_xxx_1, 0), (self.wxgui_fftsink2_0, 0))

    def get_tx_freq_var(self):
        return self.tx_freq_var

    def set_tx_freq_var(self, tx_freq_var):
        self.tx_freq_var = tx_freq_var
        self.osmosdr_sink_0.set_center_freq(self.tx_freq_var, 0)

    def get_tx_freq(self):
        return self.tx_freq

    def set_tx_freq(self, tx_freq):
        self.tx_freq = tx_freq
        self._tx_freq_slider.set_value(self.tx_freq)
        self._tx_freq_text_box.set_value(self.tx_freq)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.wxgui_fftsink2_0.set_sample_rate(self.samp_rate)
        self.osmosdr_sink_0.set_sample_rate(self.samp_rate)

    def get_audio_freq(self):
        return self.audio_freq

    def set_audio_freq(self, audio_freq):
        self.audio_freq = audio_freq


def main(top_block_cls=fm_txrx, options=None):

    tb = top_block_cls()
    tb.Start(True)
    tb.Wait()


if __name__ == '__main__':
    main()
