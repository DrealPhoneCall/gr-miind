#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: miindProbe | TimeFreq
# Author: Daryl Pongcol
# GNU Radio version: 3.7.13.4
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import miind
import sip
import sys
from gnuradio import qtgui


class qtfs_tabbed_timefreq_serial_src_fg(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "miindProbe | TimeFreq")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("miindProbe | TimeFreq")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "qtfs_tabbed_timefreq_serial_src_fg")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.update_period = update_period = 0.05
        self.serial_port = serial_port = "/dev/ttyUSB0"
        self.serial_comm = serial_comm = 'serial'
        self.samp_rate = samp_rate = 15e3
        self.num_points = num_points = 700
        self.num_channels = num_channels = 4
        self.hardware = hardware = 'nano'
        self.fft_size = fft_size = 1024
        self.center_freq = center_freq = 1e3
        self.btn_save = btn_save = 0
        self.btn_run = btn_run = 0
        self.baud_rate = baud_rate = 115200
        self.bandwidth = bandwidth = 2e3

        ##################################################
        # Blocks
        ##################################################
        self.tf_tab_widget = Qt.QTabWidget()
        self.tf_tab_widget_widget_0 = Qt.QWidget()
        self.tf_tab_widget_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tf_tab_widget_widget_0)
        self.tf_tab_widget_grid_layout_0 = Qt.QGridLayout()
        self.tf_tab_widget_layout_0.addLayout(self.tf_tab_widget_grid_layout_0)
        self.tf_tab_widget.addTab(self.tf_tab_widget_widget_0, 'Time')
        self.tf_tab_widget_widget_1 = Qt.QWidget()
        self.tf_tab_widget_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tf_tab_widget_widget_1)
        self.tf_tab_widget_grid_layout_1 = Qt.QGridLayout()
        self.tf_tab_widget_layout_1.addLayout(self.tf_tab_widget_grid_layout_1)
        self.tf_tab_widget.addTab(self.tf_tab_widget_widget_1, 'Freq')
        self.top_grid_layout.addWidget(self.tf_tab_widget, 6, 13, 2, 2)
        for r in range(6, 8):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._update_period_tool_bar = Qt.QToolBar(self)
        self._update_period_tool_bar.addWidget(Qt.QLabel('Update Prd'+": "))
        self._update_period_line_edit = Qt.QLineEdit(str(self.update_period))
        self._update_period_tool_bar.addWidget(self._update_period_line_edit)
        self._update_period_line_edit.returnPressed.connect(
        	lambda: self.set_update_period(eng_notation.str_to_num(str(self._update_period_line_edit.text().toAscii()))))
        self.tf_tab_widget_grid_layout_0.addWidget(self._update_period_tool_bar)
        self.tab_widget = Qt.QTabWidget()
        self.tab_widget_widget_0 = Qt.QWidget()
        self.tab_widget_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_widget_widget_0)
        self.tab_widget_grid_layout_0 = Qt.QGridLayout()
        self.tab_widget_layout_0.addLayout(self.tab_widget_grid_layout_0)
        self.tab_widget.addTab(self.tab_widget_widget_0, 'Time Domain')
        self.tab_widget_widget_1 = Qt.QWidget()
        self.tab_widget_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_widget_widget_1)
        self.tab_widget_grid_layout_1 = Qt.QGridLayout()
        self.tab_widget_layout_1.addLayout(self.tab_widget_grid_layout_1)
        self.tab_widget.addTab(self.tab_widget_widget_1, 'Freq Domain')
        self.top_grid_layout.addWidget(self.tab_widget, 0, 0, 14, 12)
        for r in range(0, 14):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 12):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._samp_rate_tool_bar = Qt.QToolBar(self)
        self._samp_rate_tool_bar.addWidget(Qt.QLabel('Samp Rate'+": "))
        self._samp_rate_line_edit = Qt.QLineEdit(str(self.samp_rate))
        self._samp_rate_tool_bar.addWidget(self._samp_rate_line_edit)
        self._samp_rate_line_edit.returnPressed.connect(
        	lambda: self.set_samp_rate(eng_notation.str_to_num(str(self._samp_rate_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._samp_rate_tool_bar, 4, 13, 1, 2)
        for r in range(4, 5):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._num_points_tool_bar = Qt.QToolBar(self)
        self._num_points_tool_bar.addWidget(Qt.QLabel('# of Points'+": "))
        self._num_points_line_edit = Qt.QLineEdit(str(self.num_points))
        self._num_points_tool_bar.addWidget(self._num_points_line_edit)
        self._num_points_line_edit.returnPressed.connect(
        	lambda: self.set_num_points(int(str(self._num_points_line_edit.text().toAscii()))))
        self.tf_tab_widget_grid_layout_0.addWidget(self._num_points_tool_bar)
        self._fft_size_tool_bar = Qt.QToolBar(self)
        self._fft_size_tool_bar.addWidget(Qt.QLabel('FFT Size'+": "))
        self._fft_size_line_edit = Qt.QLineEdit(str(self.fft_size))
        self._fft_size_tool_bar.addWidget(self._fft_size_line_edit)
        self._fft_size_line_edit.returnPressed.connect(
        	lambda: self.set_fft_size(int(str(self._fft_size_line_edit.text().toAscii()))))
        self.tf_tab_widget_grid_layout_1.addWidget(self._fft_size_tool_bar)
        self._center_freq_tool_bar = Qt.QToolBar(self)
        self._center_freq_tool_bar.addWidget(Qt.QLabel('Center Freq'+": "))
        self._center_freq_line_edit = Qt.QLineEdit(str(self.center_freq))
        self._center_freq_tool_bar.addWidget(self._center_freq_line_edit)
        self._center_freq_line_edit.returnPressed.connect(
        	lambda: self.set_center_freq(eng_notation.str_to_num(str(self._center_freq_line_edit.text().toAscii()))))
        self.tf_tab_widget_grid_layout_1.addWidget(self._center_freq_tool_bar)
        self._serial_port_tool_bar = Qt.QToolBar(self)
        self._serial_port_tool_bar.addWidget(Qt.QLabel('Serial Port'+": "))
        self._serial_port_line_edit = Qt.QLineEdit(str(self.serial_port))
        self._serial_port_tool_bar.addWidget(self._serial_port_line_edit)
        self._serial_port_line_edit.returnPressed.connect(
        	lambda: self.set_serial_port(str(str(self._serial_port_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._serial_port_tool_bar, 1, 13, 1, 2)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._serial_comm_options = ('serial', 'firmata', )
        self._serial_comm_labels = ('Serial', 'Firmata', )
        self._serial_comm_group_box = Qt.QGroupBox('Serial Comms')
        self._serial_comm_box = Qt.QVBoxLayout()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._serial_comm_button_group = variable_chooser_button_group()
        self._serial_comm_group_box.setLayout(self._serial_comm_box)
        for i, label in enumerate(self._serial_comm_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._serial_comm_box.addWidget(radio_button)
        	self._serial_comm_button_group.addButton(radio_button, i)
        self._serial_comm_callback = lambda i: Qt.QMetaObject.invokeMethod(self._serial_comm_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._serial_comm_options.index(i)))
        self._serial_comm_callback(self.serial_comm)
        self._serial_comm_button_group.buttonClicked[int].connect(
        	lambda i: self.set_serial_comm(self._serial_comm_options[i]))
        self.top_grid_layout.addWidget(self._serial_comm_group_box, 0, 13, 1, 2)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qts = qtgui.time_sink_f(
        	num_points, #size
        	samp_rate, #samp_rate
        	'miindProbe | Time Domain', #name
        	1 #number of inputs
        )
        self.qts.set_update_time(update_period)
        self.qts.set_y_axis(0, 5.5)

        self.qts.set_y_label('Amplitude', "")

        self.qts.enable_tags(-1, True)
        self.qts.set_trigger_mode(qtgui.TRIG_MODE_AUTO, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qts.enable_autoscale(False)
        self.qts.enable_grid(True)
        self.qts.enable_axis_labels(True)
        self.qts.enable_control_panel(False)
        self.qts.enable_stem_plot(False)

        if not True:
          self.qts.disable_legend()

        labels = ['Channel 1', 'Channel 2', 'Channel 3', 'Channel 4', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qts.set_line_label(i, "Data {0}".format(i))
            else:
                self.qts.set_line_label(i, labels[i])
            self.qts.set_line_width(i, widths[i])
            self.qts.set_line_color(i, colors[i])
            self.qts.set_line_style(i, styles[i])
            self.qts.set_line_marker(i, markers[i])
            self.qts.set_line_alpha(i, alphas[i])

        self._qts_win = sip.wrapinstance(self.qts.pyqwidget(), Qt.QWidget)
        self.tab_widget_grid_layout_0.addWidget(self._qts_win)
        self.qfs = qtgui.freq_sink_f(
        	fft_size, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	center_freq, #fc
        	samp_rate, #bw
        	'miindProbe | Frequency Domain', #name
        	1 #number of inputs
        )
        self.qfs.set_update_time(update_period)
        self.qfs.set_y_axis(-80, 30)
        self.qfs.set_y_label('Relative Gain', 'dB')
        self.qfs.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qfs.enable_autoscale(False)
        self.qfs.enable_grid(True)
        self.qfs.set_fft_average(1.0)
        self.qfs.enable_axis_labels(True)
        self.qfs.enable_control_panel(False)

        if not True:
          self.qfs.disable_legend()

        if "float" == "float" or "float" == "msg_float":
          self.qfs.set_plot_pos_half(not True)

        labels = ['Channel 1', 'Channel 2', 'Channel 3', 'Channel 4', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "black", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qfs.set_line_label(i, "Data {0}".format(i))
            else:
                self.qfs.set_line_label(i, labels[i])
            self.qfs.set_line_width(i, widths[i])
            self.qfs.set_line_color(i, colors[i])
            self.qfs.set_line_alpha(i, alphas[i])

        self._qfs_win = sip.wrapinstance(self.qfs.pyqwidget(), Qt.QWidget)
        self.tab_widget_grid_layout_1.addWidget(self._qfs_win)
        self._num_channels_tool_bar = Qt.QToolBar(self)
        self._num_channels_tool_bar.addWidget(Qt.QLabel('Num Channels'+": "))
        self._num_channels_line_edit = Qt.QLineEdit(str(self.num_channels))
        self._num_channels_tool_bar.addWidget(self._num_channels_line_edit)
        self._num_channels_line_edit.returnPressed.connect(
        	lambda: self.set_num_channels(int(str(self._num_channels_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._num_channels_tool_bar, 3, 13, 1, 2)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.miind_arduino_serial_src = miind.arduino_serial_src('/dev/ttyUSB0', 115200, int(samp_rate))
        self._hardware_options = ('nano', 'uno', )
        self._hardware_labels = ('Arduino Nano', 'Arduino Uno', )
        self._hardware_tool_bar = Qt.QToolBar(self)
        self._hardware_tool_bar.addWidget(Qt.QLabel('Hardware'+": "))
        self._hardware_combo_box = Qt.QComboBox()
        self._hardware_tool_bar.addWidget(self._hardware_combo_box)
        for label in self._hardware_labels: self._hardware_combo_box.addItem(label)
        self._hardware_callback = lambda i: Qt.QMetaObject.invokeMethod(self._hardware_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._hardware_options.index(i)))
        self._hardware_callback(self.hardware)
        self._hardware_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_hardware(self._hardware_options[i]))
        self.top_grid_layout.addWidget(self._hardware_tool_bar, 5, 13, 1, 2)
        for r in range(5, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        _btn_save_push_button = Qt.QPushButton('Save')
        self._btn_save_choices = {'Pressed': 1, 'Released': 0}
        _btn_save_push_button.pressed.connect(lambda: self.set_btn_save(self._btn_save_choices['Pressed']))
        _btn_save_push_button.released.connect(lambda: self.set_btn_save(self._btn_save_choices['Released']))
        self.top_grid_layout.addWidget(_btn_save_push_button, 12, 13, 1, 2)
        for r in range(12, 13):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        _btn_run_push_button = Qt.QPushButton('Run / Stop')
        self._btn_run_choices = {'Pressed': 1, 'Released': 0}
        _btn_run_push_button.pressed.connect(lambda: self.set_btn_run(self._btn_run_choices['Pressed']))
        _btn_run_push_button.released.connect(lambda: self.set_btn_run(self._btn_run_choices['Released']))
        self.top_grid_layout.addWidget(_btn_run_push_button, 13, 13, 1, 2)
        for r in range(13, 14):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_float*1, samp_rate,True)
        self._baud_rate_tool_bar = Qt.QToolBar(self)
        self._baud_rate_tool_bar.addWidget(Qt.QLabel('Baud Rate'+": "))
        self._baud_rate_line_edit = Qt.QLineEdit(str(self.baud_rate))
        self._baud_rate_tool_bar.addWidget(self._baud_rate_line_edit)
        self._baud_rate_line_edit.returnPressed.connect(
        	lambda: self.set_baud_rate(int(str(self._baud_rate_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._baud_rate_tool_bar, 2, 13, 1, 2)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._bandwidth_tool_bar = Qt.QToolBar(self)
        self._bandwidth_tool_bar.addWidget(Qt.QLabel('Bandwidth'+": "))
        self._bandwidth_line_edit = Qt.QLineEdit(str(self.bandwidth))
        self._bandwidth_tool_bar.addWidget(self._bandwidth_line_edit)
        self._bandwidth_line_edit.returnPressed.connect(
        	lambda: self.set_bandwidth(eng_notation.str_to_num(str(self._bandwidth_line_edit.text().toAscii()))))
        self.tf_tab_widget_grid_layout_1.addWidget(self._bandwidth_tool_bar)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_throttle_0, 0), (self.qfs, 0))
        self.connect((self.blocks_throttle_0, 0), (self.qts, 0))
        self.connect((self.miind_arduino_serial_src, 0), (self.blocks_throttle_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "qtfs_tabbed_timefreq_serial_src_fg")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_update_period(self):
        return self.update_period

    def set_update_period(self, update_period):
        self.update_period = update_period
        Qt.QMetaObject.invokeMethod(self._update_period_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.update_period)))
        self.qts.set_update_time(self.update_period)
        self.qfs.set_update_time(self.update_period)

    def get_serial_port(self):
        return self.serial_port

    def set_serial_port(self, serial_port):
        self.serial_port = serial_port
        Qt.QMetaObject.invokeMethod(self._serial_port_line_edit, "setText", Qt.Q_ARG("QString", str(self.serial_port)))

    def get_serial_comm(self):
        return self.serial_comm

    def set_serial_comm(self, serial_comm):
        self.serial_comm = serial_comm
        self._serial_comm_callback(self.serial_comm)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        Qt.QMetaObject.invokeMethod(self._samp_rate_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.samp_rate)))
        self.qts.set_samp_rate(self.samp_rate)
        self.qfs.set_frequency_range(self.center_freq, self.samp_rate)
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_num_points(self):
        return self.num_points

    def set_num_points(self, num_points):
        self.num_points = num_points
        Qt.QMetaObject.invokeMethod(self._num_points_line_edit, "setText", Qt.Q_ARG("QString", str(self.num_points)))

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels
        Qt.QMetaObject.invokeMethod(self._num_channels_line_edit, "setText", Qt.Q_ARG("QString", str(self.num_channels)))

    def get_hardware(self):
        return self.hardware

    def set_hardware(self, hardware):
        self.hardware = hardware
        self._hardware_callback(self.hardware)

    def get_fft_size(self):
        return self.fft_size

    def set_fft_size(self, fft_size):
        self.fft_size = fft_size
        Qt.QMetaObject.invokeMethod(self._fft_size_line_edit, "setText", Qt.Q_ARG("QString", str(self.fft_size)))

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        Qt.QMetaObject.invokeMethod(self._center_freq_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.center_freq)))
        self.qfs.set_frequency_range(self.center_freq, self.samp_rate)

    def get_btn_save(self):
        return self.btn_save

    def set_btn_save(self, btn_save):
        self.btn_save = btn_save

    def get_btn_run(self):
        return self.btn_run

    def set_btn_run(self, btn_run):
        self.btn_run = btn_run

    def get_baud_rate(self):
        return self.baud_rate

    def set_baud_rate(self, baud_rate):
        self.baud_rate = baud_rate
        Qt.QMetaObject.invokeMethod(self._baud_rate_line_edit, "setText", Qt.Q_ARG("QString", str(self.baud_rate)))

    def get_bandwidth(self):
        return self.bandwidth

    def set_bandwidth(self, bandwidth):
        self.bandwidth = bandwidth
        Qt.QMetaObject.invokeMethod(self._bandwidth_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.bandwidth)))


def main(top_block_cls=qtfs_tabbed_timefreq_serial_src_fg, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
