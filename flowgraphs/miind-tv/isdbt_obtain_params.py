#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: miind | ISDBT Obtain Parameters
# GNU Radio version: 3.7.13.5
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from optparse import OptionParser
import isdbt
import pmt
import sip
import sys
from gnuradio import qtgui


class isdbt_obtain_params(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "miind | ISDBT Obtain Parameters")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("miind | ISDBT Obtain Parameters")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "isdbt_obtain_params")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.mode = mode = 3
        self.total_carriers = total_carriers = 2**(10+mode)
        self.samp_rate = samp_rate = 8e6*64/63
        self.inter = inter = 64
        self.guard = guard = 1/16.0
        self.decim = decim = 63
        self.data_carriers = data_carriers = 13*96*2**(mode-1)
        self.center_freq = center_freq = 629.143e6
        self.active_carriers = active_carriers = 13*108*2**(mode-1)+1

        ##################################################
        # Blocks
        ##################################################
        self._samp_rate_tool_bar = Qt.QToolBar(self)
        self._samp_rate_tool_bar.addWidget(Qt.QLabel('Samp Rate'+": "))
        self._samp_rate_line_edit = Qt.QLineEdit(str(self.samp_rate))
        self._samp_rate_tool_bar.addWidget(self._samp_rate_line_edit)
        self._samp_rate_line_edit.returnPressed.connect(
        	lambda: self.set_samp_rate(eng_notation.str_to_num(str(self._samp_rate_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._samp_rate_tool_bar, 0, 13, 1, 2)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._guard_options = (1/4.0, 1/8.0, 1/16.0, 1/32.0, )
        self._guard_labels = ('1/4', '1/8', '1/16', '1/32', )
        self._guard_tool_bar = Qt.QToolBar(self)
        self._guard_tool_bar.addWidget(Qt.QLabel('Guard Interval'+": "))
        self._guard_combo_box = Qt.QComboBox()
        self._guard_tool_bar.addWidget(self._guard_combo_box)
        for label in self._guard_labels: self._guard_combo_box.addItem(label)
        self._guard_callback = lambda i: Qt.QMetaObject.invokeMethod(self._guard_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._guard_options.index(i)))
        self._guard_callback(self.guard)
        self._guard_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_guard(self._guard_options[i]))
        self.top_grid_layout.addWidget(self._guard_tool_bar, 3, 13, 1, 2)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qt_time_sink = qtgui.time_sink_f(
        	2*int(total_carriers*(1+guard)), #size
        	samp_rate, #samp_rate
        	'miind | TV', #name
        	1 #number of inputs
        )
        self.qt_time_sink.set_update_time(0.10)
        self.qt_time_sink.set_y_axis(-1, 1)

        self.qt_time_sink.set_y_label('Amplitude', '')

        self.qt_time_sink.enable_tags(-1, True)
        self.qt_time_sink.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qt_time_sink.enable_autoscale(True)
        self.qt_time_sink.enable_grid(False)
        self.qt_time_sink.enable_axis_labels(True)
        self.qt_time_sink.enable_control_panel(False)
        self.qt_time_sink.enable_stem_plot(False)

        if not False:
          self.qt_time_sink.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qt_time_sink.set_line_label(i, "Data {0}".format(i))
            else:
                self.qt_time_sink.set_line_label(i, labels[i])
            self.qt_time_sink.set_line_width(i, widths[i])
            self.qt_time_sink.set_line_color(i, colors[i])
            self.qt_time_sink.set_line_style(i, styles[i])
            self.qt_time_sink.set_line_marker(i, markers[i])
            self.qt_time_sink.set_line_alpha(i, alphas[i])

        self._qt_time_sink_win = sip.wrapinstance(self.qt_time_sink.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qt_time_sink_win, 0, 0, 6, 12)
        for r in range(0, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 12):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qt_freq_sink = qtgui.freq_sink_c(
        	1024, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	0, #fc
        	samp_rate, #bw
        	"Incoming spectrum", #name
        	1 #number of inputs
        )
        self.qt_freq_sink.set_update_time(0.10)
        self.qt_freq_sink.set_y_axis(-140, 10)
        self.qt_freq_sink.set_y_label('Relative Gain', 'dB')
        self.qt_freq_sink.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qt_freq_sink.enable_autoscale(False)
        self.qt_freq_sink.enable_grid(False)
        self.qt_freq_sink.set_fft_average(1.0)
        self.qt_freq_sink.enable_axis_labels(True)
        self.qt_freq_sink.enable_control_panel(False)

        if not False:
          self.qt_freq_sink.disable_legend()

        if "complex" == "float" or "complex" == "msg_float":
          self.qt_freq_sink.set_plot_pos_half(not True)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qt_freq_sink.set_line_label(i, "Data {0}".format(i))
            else:
                self.qt_freq_sink.set_line_label(i, labels[i])
            self.qt_freq_sink.set_line_width(i, widths[i])
            self.qt_freq_sink.set_line_color(i, colors[i])
            self.qt_freq_sink.set_line_alpha(i, alphas[i])

        self._qt_freq_sink_win = sip.wrapinstance(self.qt_freq_sink.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qt_freq_sink_win, 6, 0, 6, 12)
        for r in range(6, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 12):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._mode_options = (3, 2, 1, )
        self._mode_labels = ('3 (8k)', '2 (4k)', '1 (2k)', )
        self._mode_tool_bar = Qt.QToolBar(self)
        self._mode_tool_bar.addWidget(Qt.QLabel('Mode'+": "))
        self._mode_combo_box = Qt.QComboBox()
        self._mode_tool_bar.addWidget(self._mode_combo_box)
        for label in self._mode_labels: self._mode_combo_box.addItem(label)
        self._mode_callback = lambda i: Qt.QMetaObject.invokeMethod(self._mode_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._mode_options.index(i)))
        self._mode_callback(self.mode)
        self._mode_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_mode(self._mode_options[i]))
        self.top_grid_layout.addWidget(self._mode_tool_bar, 2, 13, 1, 2)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.low_pass_filter_0 = filter.fir_filter_ccf(1, firdes.low_pass(
        	1, samp_rate, 5.8e6/2.0, 0.5e6, firdes.WIN_HAMMING, 6.76))
        self.isdbt_tmcc_decoder_0 = isdbt.tmcc_decoder(3, True)
        self.isdbt_sync_and_channel_estimation_0 = isdbt.sync_and_channel_estimation(8192, 5617, 10)
        self.isdbt_ofdm_sym_acquisition_0 = isdbt.ofdm_sym_acquisition(total_carriers, int(guard*total_carriers), 10)
        self.fft_vxx_0 = fft.fft_vcc(total_carriers, True, (window.rectangular(total_carriers)), True, 1)
        self._center_freq_tool_bar = Qt.QToolBar(self)
        self._center_freq_tool_bar.addWidget(Qt.QLabel('Center Freq'+": "))
        self._center_freq_line_edit = Qt.QLineEdit(str(self.center_freq))
        self._center_freq_tool_bar.addWidget(self._center_freq_line_edit)
        self._center_freq_line_edit.returnPressed.connect(
        	lambda: self.set_center_freq(eng_notation.str_to_num(str(self._center_freq_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._center_freq_tool_bar, 1, 13, 1, 2)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_null_sink_0_0 = blocks.null_sink(gr.sizeof_gr_complex*1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_gr_complex*data_carriers)
        self.blocks_file_source_1_0 = blocks.file_source(gr.sizeof_gr_complex*1, '/home/ergwd-pcieerd/Projects/gr-miind/flowgraphs/miind-tv/recordings/569MHz_recording.dat', True)
        self.blocks_file_source_1_0.set_begin_tag(pmt.PMT_NIL)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_file_source_1_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.isdbt_sync_and_channel_estimation_0, 0))
        self.connect((self.isdbt_ofdm_sym_acquisition_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.isdbt_sync_and_channel_estimation_0, 0), (self.isdbt_tmcc_decoder_0, 0))
        self.connect((self.isdbt_tmcc_decoder_0, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.isdbt_ofdm_sym_acquisition_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "isdbt_obtain_params")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_mode(self):
        return self.mode

    def set_mode(self, mode):
        self.mode = mode
        self.set_total_carriers(2**(10+self.mode))
        self.set_data_carriers(13*96*2**(self.mode-1))
        self._mode_callback(self.mode)
        self.set_active_carriers(13*108*2**(self.mode-1)+1)

    def get_total_carriers(self):
        return self.total_carriers

    def set_total_carriers(self, total_carriers):
        self.total_carriers = total_carriers

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        Qt.QMetaObject.invokeMethod(self._samp_rate_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.samp_rate)))
        self.qt_time_sink.set_samp_rate(self.samp_rate)
        self.qt_freq_sink.set_frequency_range(0, self.samp_rate)
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, 5.8e6/2.0, 0.5e6, firdes.WIN_HAMMING, 6.76))
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_inter(self):
        return self.inter

    def set_inter(self, inter):
        self.inter = inter

    def get_guard(self):
        return self.guard

    def set_guard(self, guard):
        self.guard = guard
        self._guard_callback(self.guard)

    def get_decim(self):
        return self.decim

    def set_decim(self, decim):
        self.decim = decim

    def get_data_carriers(self):
        return self.data_carriers

    def set_data_carriers(self, data_carriers):
        self.data_carriers = data_carriers

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        Qt.QMetaObject.invokeMethod(self._center_freq_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.center_freq)))

    def get_active_carriers(self):
        return self.active_carriers

    def set_active_carriers(self, active_carriers):
        self.active_carriers = active_carriers


def main(top_block_cls=isdbt_obtain_params, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
