#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: miind | ISDBT Instrumentation
# GNU Radio version: 3.7.13.4
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import isdbt
import mer
import numpy as np
import pmt
import sip
import sys
from gnuradio import qtgui


class isdbt_rx_instr(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "miind | ISDBT Instrumentation")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("miind | ISDBT Instrumentation")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "isdbt_rx_instr")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.mode = mode = 3
        self.total_carriers = total_carriers = 2**(10+mode)
        self.symbol_table_QPSK = symbol_table_QPSK = (1+1j, 1-1j, -1+1j, -1-1j)/np.sqrt(2)
        self.symbol_table_64QAM = symbol_table_64QAM = (7+7j, 7+5j, 5+7j, 5+5j, 7+1j, 7+3j, 5+1j, 5+3j, 1+7j, 1+5j, 3+7j, 3+5j, 1+1j, 1+3j, 3+1j, 3+3j, 7-7j, 7-5j, 5-7j, 5-5j, 7-1j, 7-3j, 5-1j, 5-3j, 1-7j, 1-5j, 3-7j, 3-5j, 1-1j, 1-3j, 3-1j, 3-3j, -7+7j, -7+5j, -5+7j, -5+5j, -7+1j, -7+3j, -5+1j, -5+3j, -1+7j, -1+5j, -3+7j, -3+5j, -1+1j, -1+3j, -3+1j, -3+3j, -7-7j, -7-5j, -5-7j, -5-5j, -7-1j, -7-3j, -5-1j, -5-3j, -1-7j, -1-5j, -3-7j, -3-5j, -1-1j, -1-3j, -3-1j, -3-3j)/np.sqrt(42)
        self.symbol_table_16QAM = symbol_table_16QAM = (3+3j, 3+1j, 1+3j, 1+1j, 3-3j, 3-1j, 1-3j, 1-1j, -3+3j, -3+1j, -1+3j, -1+1j, -3-3j, -3-1j, -1-3j, -1-1j)/np.sqrt(10)
        self.samp_usrp = samp_usrp = 8e6
        self.samp_rate = samp_rate = 6.4e6*80/63
        self.inter = inter = 64
        self.guard = guard = 1/16.0
        self.decim = decim = 63
        self.data_carriers = data_carriers = 13*96*2**(mode-1)
        self.center_freq = center_freq = 629.143e6
        self.active_carriers = active_carriers = 13*108*2**(mode-1)+1

        ##################################################
        # Blocks
        ##################################################
        self._samp_rate_tool_bar = Qt.QToolBar(self)
        self._samp_rate_tool_bar.addWidget(Qt.QLabel('Samp Rate'+": "))
        self._samp_rate_line_edit = Qt.QLineEdit(str(self.samp_rate))
        self._samp_rate_tool_bar.addWidget(self._samp_rate_line_edit)
        self._samp_rate_line_edit.returnPressed.connect(
        	lambda: self.set_samp_rate(eng_notation.str_to_num(str(self._samp_rate_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._samp_rate_tool_bar, 1, 13, 1, 2)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._mode_options = (3, 2, 1, )
        self._mode_labels = ('3 (8k)', '2 (4k)', '1 (2k)', )
        self._mode_tool_bar = Qt.QToolBar(self)
        self._mode_tool_bar.addWidget(Qt.QLabel('Mode'+": "))
        self._mode_combo_box = Qt.QComboBox()
        self._mode_tool_bar.addWidget(self._mode_combo_box)
        for label in self._mode_labels: self._mode_combo_box.addItem(label)
        self._mode_callback = lambda i: Qt.QMetaObject.invokeMethod(self._mode_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._mode_options.index(i)))
        self._mode_callback(self.mode)
        self._mode_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_mode(self._mode_options[i]))
        self.top_grid_layout.addWidget(self._mode_tool_bar, 3, 13, 1, 2)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._guard_options = (1/4.0, 1/8.0, 1/16.0, 1/32.0, )
        self._guard_labels = ('1/4', '1/8', '1/16', '1/32', )
        self._guard_tool_bar = Qt.QToolBar(self)
        self._guard_tool_bar.addWidget(Qt.QLabel('Guard Interval'+": "))
        self._guard_combo_box = Qt.QComboBox()
        self._guard_tool_bar.addWidget(self._guard_combo_box)
        for label in self._guard_labels: self._guard_combo_box.addItem(label)
        self._guard_callback = lambda i: Qt.QMetaObject.invokeMethod(self._guard_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._guard_options.index(i)))
        self._guard_callback(self.guard)
        self._guard_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_guard(self._guard_options[i]))
        self.top_grid_layout.addWidget(self._guard_tool_bar, 2, 13, 1, 2)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_time_sink_x_0_0 = qtgui.time_sink_f(
        	active_carriers, #size
        	samp_rate, #samp_rate
        	"Channel Taps", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0_0.set_y_axis(-3.2, 3.2)

        self.qtgui_time_sink_x_0_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0_0.enable_tags(-1, False)
        self.qtgui_time_sink_x_0_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0_0.enable_grid(False)
        self.qtgui_time_sink_x_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0_0.enable_stem_plot(False)

        if not False:
          self.qtgui_time_sink_x_0_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_0_win, 6, 0, 6, 6)
        for r in range(6, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 6):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_const_sink_x_0 = qtgui.const_sink_c(
        	1024, #size
        	"Constellation", #name
        	1 #number of inputs
        )
        self.qtgui_const_sink_x_0.set_update_time(0.10)
        self.qtgui_const_sink_x_0.set_y_axis(-2, 2)
        self.qtgui_const_sink_x_0.set_x_axis(-2, 2)
        self.qtgui_const_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, "")
        self.qtgui_const_sink_x_0.enable_autoscale(False)
        self.qtgui_const_sink_x_0.enable_grid(True)
        self.qtgui_const_sink_x_0.enable_axis_labels(True)

        if not False:
          self.qtgui_const_sink_x_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "red", "red", "red",
                  "red", "red", "red", "red", "red"]
        styles = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        markers = [0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_const_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_const_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_const_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_const_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_const_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_const_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_const_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_const_sink_x_0_win = sip.wrapinstance(self.qtgui_const_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_const_sink_x_0_win, 0, 0, 6, 6)
        for r in range(0, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 6):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qt_numsink_ste = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_VERT,
            3
        )
        self.qt_numsink_ste.set_update_time(0.10)
        self.qt_numsink_ste.set_title("")

        labels = ['Deviation', 'Average', 'STE', '', '',
                  '', '', '', '', '']
        units = ['', '', '', '', '',
                 '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
                  ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        for i in xrange(3):
            self.qt_numsink_ste.set_min(i, 0)
            self.qt_numsink_ste.set_max(i, 1)
            self.qt_numsink_ste.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qt_numsink_ste.set_label(i, "Data {0}".format(i))
            else:
                self.qt_numsink_ste.set_label(i, labels[i])
            self.qt_numsink_ste.set_unit(i, units[i])
            self.qt_numsink_ste.set_factor(i, factor[i])

        self.qt_numsink_ste.enable_autoscale(False)
        self._qt_numsink_ste_win = sip.wrapinstance(self.qt_numsink_ste.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qt_numsink_ste_win, 6, 6, 6, 3)
        for r in range(6, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(6, 9):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qt_numsink_err_rate = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_VERT,
            2
        )
        self.qt_numsink_err_rate.set_update_time(0.10)
        self.qt_numsink_err_rate.set_title(" ")

        labels = ['BER - Viterbi', 'BER - RS', 'BER - Viterbi', '', '',
                  '', '', '', '', '']
        units = ['', '', '', '', '',
                 '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
                  ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        for i in xrange(2):
            self.qt_numsink_err_rate.set_min(i, -10)
            self.qt_numsink_err_rate.set_max(i, 0)
            self.qt_numsink_err_rate.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qt_numsink_err_rate.set_label(i, "Data {0}".format(i))
            else:
                self.qt_numsink_err_rate.set_label(i, labels[i])
            self.qt_numsink_err_rate.set_unit(i, units[i])
            self.qt_numsink_err_rate.set_factor(i, factor[i])

        self.qt_numsink_err_rate.enable_autoscale(False)
        self._qt_numsink_err_rate_win = sip.wrapinstance(self.qt_numsink_err_rate.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qt_numsink_err_rate_win, 0, 7, 6, 2)
        for r in range(0, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(7, 9):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qt_numsink_ = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_VERT,
            1
        )
        self.qt_numsink_.set_update_time(0.10)
        self.qt_numsink_.set_title(" ")

        labels = ['MER', '', '', '', '',
                  '', '', '', '', '']
        units = ['', '', '', '', '',
                 '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
                  ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        for i in xrange(1):
            self.qt_numsink_.set_min(i, 0)
            self.qt_numsink_.set_max(i, 50)
            self.qt_numsink_.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qt_numsink_.set_label(i, "Data {0}".format(i))
            else:
                self.qt_numsink_.set_label(i, labels[i])
            self.qt_numsink_.set_unit(i, units[i])
            self.qt_numsink_.set_factor(i, factor[i])

        self.qt_numsink_.enable_autoscale(False)
        self._qt_numsink__win = sip.wrapinstance(self.qt_numsink_.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qt_numsink__win, 0, 6, 6, 1)
        for r in range(0, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(6, 7):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.mer_probe_ste_cf_0_0 = mer.probe_ste_cf((symbol_table_64QAM), 0.05)
        self.mer_probe_mer_c_0_0 = mer.probe_mer_c((symbol_table_64QAM),0.05)
        self.low_pass_filter_0 = filter.fir_filter_ccf(1, firdes.low_pass(
        	1, samp_rate, 5.8e6/2.0, 0.5e6, firdes.WIN_HAMMING, 6.76))
        self.isdbt_time_deinterleaver_0 = isdbt.time_deinterleaver(3, 1, 4, 12, 2, 0, 0)
        self.isdbt_symbol_demapper_0 = isdbt.symbol_demapper(3, 1, 4, 12, 64, 0, 64)
        self.isdbt_subset_of_carriers_0 = isdbt.subset_of_carriers(96*4*13, 96*4*1, 13*96*4-1)
        self.isdbt_isdbt_rf_channel_decoding_0 = isdbt.isdbt_rf_channel_decoding(
            max_freq_offset=31,
            guard=0.0625,
            mode=3,
            snr=10,
            tmcc_print=False,
        )
        self.isdbt_frequency_deinterleaver_0 = isdbt.frequency_deinterleaver(True, 3)
        self.isdbt_channel_decoding_0 = isdbt.isdbt_channel_decoding(
            layer_segments=12,
            mode=3,
            constellation_size=64,
            rate=2,
        )
        self._center_freq_tool_bar = Qt.QToolBar(self)
        self._center_freq_tool_bar.addWidget(Qt.QLabel('Center Freq'+": "))
        self._center_freq_line_edit = Qt.QLineEdit(str(self.center_freq))
        self._center_freq_tool_bar.addWidget(self._center_freq_line_edit)
        self._center_freq_line_edit.returnPressed.connect(
        	lambda: self.set_center_freq(eng_notation.str_to_num(str(self._center_freq_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._center_freq_tool_bar, 0, 13, 1, 2)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_vector_to_stream_0_2 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, 96*4*12)
        self.blocks_vector_to_stream_0_0 = blocks.vector_to_stream(gr.sizeof_float*1, active_carriers)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_null_sink_1_1 = blocks.null_sink(gr.sizeof_char*384)
        self.blocks_nlog10_ff_0_1 = blocks.nlog10_ff(1, active_carriers, 0)
        self.blocks_nlog10_ff_0_0 = blocks.nlog10_ff(1, 1, 0)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(1, 1, 0)
        self.blocks_keep_one_in_n_0 = blocks.keep_one_in_n(gr.sizeof_gr_complex*1, 14)
        self.blocks_file_source_1_0 = blocks.file_source(gr.sizeof_gr_complex*1, '/home/ergwd-pcieerd/Projects/gr-miind/flowgraphs/miind-tv/recordings/569MHz_recording.dat', True)
        self.blocks_file_source_1_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_file_sink_0_1 = blocks.file_sink(gr.sizeof_char*1, 'test_out.ts', False)
        self.blocks_file_sink_0_1.set_unbuffered(False)
        self.blocks_complex_to_mag_0 = blocks.complex_to_mag(active_carriers)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_complex_to_mag_0, 0), (self.blocks_nlog10_ff_0_1, 0))
        self.connect((self.blocks_file_source_1_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_keep_one_in_n_0, 0), (self.mer_probe_mer_c_0_0, 0))
        self.connect((self.blocks_keep_one_in_n_0, 0), (self.mer_probe_ste_cf_0_0, 0))
        self.connect((self.blocks_keep_one_in_n_0, 0), (self.qtgui_const_sink_x_0, 0))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.qt_numsink_err_rate, 1))
        self.connect((self.blocks_nlog10_ff_0_0, 0), (self.qt_numsink_err_rate, 0))
        self.connect((self.blocks_nlog10_ff_0_1, 0), (self.blocks_vector_to_stream_0_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.blocks_vector_to_stream_0_0, 0), (self.qtgui_time_sink_x_0_0, 0))
        self.connect((self.blocks_vector_to_stream_0_2, 0), (self.blocks_keep_one_in_n_0, 0))
        self.connect((self.isdbt_channel_decoding_0, 0), (self.blocks_file_sink_0_1, 0))
        self.connect((self.isdbt_channel_decoding_0, 1), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.isdbt_channel_decoding_0, 2), (self.blocks_nlog10_ff_0_0, 0))
        self.connect((self.isdbt_frequency_deinterleaver_0, 0), (self.isdbt_time_deinterleaver_0, 0))
        self.connect((self.isdbt_isdbt_rf_channel_decoding_0, 1), (self.blocks_complex_to_mag_0, 0))
        self.connect((self.isdbt_isdbt_rf_channel_decoding_0, 0), (self.isdbt_frequency_deinterleaver_0, 0))
        self.connect((self.isdbt_isdbt_rf_channel_decoding_0, 0), (self.isdbt_subset_of_carriers_0, 0))
        self.connect((self.isdbt_subset_of_carriers_0, 0), (self.blocks_vector_to_stream_0_2, 0))
        self.connect((self.isdbt_symbol_demapper_0, 0), (self.blocks_null_sink_1_1, 0))
        self.connect((self.isdbt_symbol_demapper_0, 1), (self.isdbt_channel_decoding_0, 0))
        self.connect((self.isdbt_time_deinterleaver_0, 0), (self.isdbt_symbol_demapper_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.isdbt_isdbt_rf_channel_decoding_0, 0))
        self.connect((self.mer_probe_mer_c_0_0, 0), (self.qt_numsink_, 0))
        self.connect((self.mer_probe_ste_cf_0_0, 0), (self.qt_numsink_ste, 0))
        self.connect((self.mer_probe_ste_cf_0_0, 2), (self.qt_numsink_ste, 2))
        self.connect((self.mer_probe_ste_cf_0_0, 1), (self.qt_numsink_ste, 1))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "isdbt_rx_instr")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_mode(self):
        return self.mode

    def set_mode(self, mode):
        self.mode = mode
        self._mode_callback(self.mode)
        self.set_active_carriers(13*108*2**(self.mode-1)+1)
        self.set_total_carriers(2**(10+self.mode))
        self.set_data_carriers(13*96*2**(self.mode-1))

    def get_total_carriers(self):
        return self.total_carriers

    def set_total_carriers(self, total_carriers):
        self.total_carriers = total_carriers

    def get_symbol_table_QPSK(self):
        return self.symbol_table_QPSK

    def set_symbol_table_QPSK(self, symbol_table_QPSK):
        self.symbol_table_QPSK = symbol_table_QPSK

    def get_symbol_table_64QAM(self):
        return self.symbol_table_64QAM

    def set_symbol_table_64QAM(self, symbol_table_64QAM):
        self.symbol_table_64QAM = symbol_table_64QAM

    def get_symbol_table_16QAM(self):
        return self.symbol_table_16QAM

    def set_symbol_table_16QAM(self, symbol_table_16QAM):
        self.symbol_table_16QAM = symbol_table_16QAM

    def get_samp_usrp(self):
        return self.samp_usrp

    def set_samp_usrp(self, samp_usrp):
        self.samp_usrp = samp_usrp

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        Qt.QMetaObject.invokeMethod(self._samp_rate_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.samp_rate)))
        self.qtgui_time_sink_x_0_0.set_samp_rate(self.samp_rate)
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, 5.8e6/2.0, 0.5e6, firdes.WIN_HAMMING, 6.76))
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_inter(self):
        return self.inter

    def set_inter(self, inter):
        self.inter = inter

    def get_guard(self):
        return self.guard

    def set_guard(self, guard):
        self.guard = guard
        self._guard_callback(self.guard)

    def get_decim(self):
        return self.decim

    def set_decim(self, decim):
        self.decim = decim

    def get_data_carriers(self):
        return self.data_carriers

    def set_data_carriers(self, data_carriers):
        self.data_carriers = data_carriers

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        Qt.QMetaObject.invokeMethod(self._center_freq_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.center_freq)))

    def get_active_carriers(self):
        return self.active_carriers

    def set_active_carriers(self, active_carriers):
        self.active_carriers = active_carriers


def main(top_block_cls=isdbt_rx_instr, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
