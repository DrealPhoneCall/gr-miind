import os
from subprocess import call
from variables.params import get_channels, get_param_json

class mixin_buttons():
    def set_btn_prev(self, btn_prev):
        if btn_prev:
            print "btn_prev callback"
            try:
                self.current_channel_index -= 1
                channel = self.channels[self.current_channel_index]
            except:
                self.current_channel_index = len(self.channels)-1
                channel = self.channels[self.current_channel_index]
            self.variables = get_param_json(channel)
            self.set_channel_params()

    def set_btn_next(self, btn_next):
        if btn_next:
            print "btn_next callback"
            try:
                self.current_channel_index += 1
                channel = self.channels[self.current_channel_index]
            except:
                self.current_channel_index = 0
                channel = self.channels[self.current_channel_index]
            self.variables = get_param_json(channel)
            self.set_channel_params()

    def set_btn_play(self, btn_play):
        cwd = os.getcwd()
        file_path = '%s/flowgraphs/miind-tv/output-recordings/test_out.ts' %(cwd)
        print "btn_play callback"
        call(['ffplay', file_path]) 

class mixin_params():
    def __init__(self):
        self.channels = get_channels()
        self.current_channel_index = 0
        self.variables = get_param_json('channel_01')

    def set_channel_params(self):
        self.set_center_freq(self.variables['center_freq'])
        self.set_guard(self.variables['guard'])
        self.set_mode(self.variables['mode'])
        self.set_samp_rate(self.variables['samp_rate'])