import os


def get_variables(line, dump_file):
    dumps_path = '/home/ergwd/Projects/gr-miindprobe/apps/data_dumps'
    if line == 'tx':
        path = '%s/%s_tx.dat' % (str(dumps_path), dump_file)
    else:
        path = '%s/%s_rx.dat' % (str(dumps_path), dump_file)
    try:
        f = open(path, 'w')
        f.close()
    except:
        f = None
    return str(path)


def get_or_create_blk(blks_class, blks_attr, blk_name='Null'):
    def decorated(f):
        def wrapper(*args, **kwargs):
            try:
                f(*args, **kwargs)
            except Exception as e:
                print "Error initiating %s block: %s" % (blk, e.message)
                for key, value in kwargs.iteritems():
                    blks_class.__dict__[key] = blks_attr.__dict__[key]
        return wrapper
    return decorated