#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: GPL-3.0
#
##################################################
# GNU Radio Python Flow Graph
# Title: miind | ISDBT Obtain Parameters
# Generated: Fri Nov  2 22:02:39 2018
# GNU Radio version: 3.7.12.0
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

import argparse

from PyQt4 import Qt
from gnuradio import gr
from gnuradio import qtgui

from isdbt_obtain_params import isdbt_obtain_params
from mixins import mixin_buttons, mixin_params

class main_obtain_params(isdbt_obtain_params, mixin_buttons, mixin_params):

    def __init__(self, args):
        self.args = args
        self.guard = self.get_guard_float(self.args.guard)
        self.center_freq = self.get_channel_float(self.args.channel)
        isdbt_obtain_params.__init__(self, self.args.mode, self.guard, self.center_freq)
        mixin_params.__init__(self)

    def get_guard_float(self, guard):
        if guard == 1:
            return 250e-3
        elif guard == 2:
            return 125e-3
        elif guard == 3:
            return 62.5e-3
        elif guard == 4:
            return 31.25e-3
        else:
            return 250e-3

    def get_channel_float(self, channel):
        if channel == 1:
            return 629.143e6
        elif channel == 2:
            return 521.143e6
        else:
            return 629.143e6

    def set_mode(self, mode):
        print "Setting the transmission mode...\n\n"
        isdbt_obtain_params.set_mode(self, mode)

        self.isdbt_ofdm_sym_acquisition.set_mode(mode)
        self.isdbt_sync_and_channel_estimation.set_mode(mode)
        self.isdbt_tmcc_decoder.set_mode(mode)
        
        print "Blocks: isdbt_ofdm_sym_acquisition, isdbt_tmcc_decoder,"
        print "and isdbt_sync_and_channel_estimation have been switched to " + int(mode)

        self.set_total_carriers(self.total_carriers)
        self.set_active_carriers(self.active_carriers)
        self.set_data_carriers(self.data_carriers)

    def set_total_carriers(self, total_carriers):
        isdbt_obtain_params.set_total_carriers(self, total_carriers)
        self.isdbt_ofdm_sym_acquisition.d_fft_length = total_carriers
        self.isdbt_ofdm_sym_acquisition.d_cp_length = int(self.guard*total_carriers)
        self.fft_vxx_rx.set_fft_size(total_carriers)
        self.fft_vxx_rx.set_window_size((window.rectangular(total_carriers)))

    def set_active_carriers(self, active_carriers):
        isdbt_obtain_params.set_active_carriers(self, active_carriers)
        self.qtgui_const_sink.set_num_points(active_carriers)

    def set_data_carriers(self, data_carriers):
        isdbt_obtain_params.set_data_carriers(self, data_carriers)
        self.blocks_vector_to_stream_rx.set_num_items(data_carriers)

    def set_guard(self, guard):
        print "Setting the guard interval...\n\n"
        isdbt_obtain_params.set_guard(self, guard)
        self.isdbt_ofdm_sym_acquisition.set_cp_length(int(guard*self.total_carriers))

    def set_channel_params(self):
        mixin_params.set_channel_params(self)

    def set_btn_prev(self, btn_prev):
        isdbt_obtain_params.set_btn_prev(self, btn_prev)
        mixin_buttons.set_btn_prev(self, btn_prev)

    def set_btn_next(self, btn_next):
        isdbt_obtain_params.set_btn_next(self, btn_next)
        mixin_buttons.set_btn_next(self, btn_next)

    def set_btn_play(self, btn_play):
        isdbt_obtain_params.set_btn_play(self, btn_play)
        mixin_buttons.set_btn_play(self, btn_play)


def options():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument ("-m", "--mode", type=int, default=3,
                        help="set the transmission mode [default=%default]")
    parser.add_argument ("-g", "--guard", type=int, default=3,
                        help="set the guard interval [default=%default]")
    parser.add_argument ("-ch", "--channel", type=int, default=1,
                        help="set the channel frequency [default=%default]")
    
    return parser

def main(top_block_cls=main_obtain_params, options=options):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    # instantiate args and app
    parser = options()
    args = parser.parse_args() 

    tb = top_block_cls(args)
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
