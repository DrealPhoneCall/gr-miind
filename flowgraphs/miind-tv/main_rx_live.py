#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: GPL-3.0
#
##################################################
# GNU Radio Python Flow Graph
# Title: miind | ISDBT Obtain Parameters
# Generated: Fri Nov  2 22:02:39 2018
# GNU Radio version: 3.7.12.0
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import gr
from gnuradio import qtgui

from isdbt_rx_live import isdbt_rx_live
from mixins import mixin_buttons, mixin_params


class main_rx_live(isdbt_rx_live, mixin_buttons, mixin_params):

    def __init__(self):
        isdbt_rx_live.__init__(self)
        mixin_params.__init__(self)

    def set_mode(self, mode):
        isdbt_rx_live.set_mode(self, mode)
        # mixin_params.set_mode(self, mode)
        print "Setting the transmission mode...\n\n"
        self.isdbt_sync_and_channel_estimation.set_mode(mode)
        self.isdbt_tmcc_decoder.set_mode(mode)
        self.isdbt_frequency_deinterleaver.set_mode(mode)
        self.isdbt_time_deinterleaver.set_mode(mode)
        self.isdbt_symbol_demapper.set_mode(mode)
        self.isdbt_bit_deinterleaver.set_mode(mode)
        print "Blocks: isdbt_sync_and_channel_estimation, isdbt_tmcc_decoder,"
        print "isdbt_frequency_deinterleaver, isdbt_time_deinterleaver,"
        print "isdbt_symbol_demapper and isdbt_bit_deinterleaver have been switched to " + int(mode)

        self.set_total_carriers(self.total_carriers)
        self.set_active_carriers(self.active_carriers)
        self.set_data_carriers(self.data_carriers)

    def set_guard(self, guard):
        isdbt_rx_live.set_guard(self, guard)
        mixin_params.set_guard(self, guard)

    def set_channel_params(self):
        mixin_params.set_channel_params(self)

    def set_btn_prev(self, btn_prev):
        isdbt_rx_live.set_btn_prev(self, btn_prev)
        mixin_buttons.set_btn_prev(self, btn_prev)

    def set_btn_next(self, btn_next):
        isdbt_rx_live.set_btn_next(self, btn_next)
        mixin_buttons.set_btn_next(self, btn_next)

    def set_btn_play(self, btn_play):
        isdbt_rx_live.set_btn_play(self, btn_play)
        mixin_buttons.set_btn_play(self, btn_play)


def main(top_block_cls=main_rx_live, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
