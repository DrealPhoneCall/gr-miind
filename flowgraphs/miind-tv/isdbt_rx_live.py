#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: miind | TV Receive
# GNU Radio version: 3.7.13.5
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from optparse import OptionParser
import isdbt
import pmt
import sip
import sys
from gnuradio import qtgui


class isdbt_rx_live(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "miind | TV Receive")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("miind | TV Receive")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "isdbt_rx_live")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.mode = mode = 3
        self.total_carriers = total_carriers = 2**(10+mode)
        self.samp_rate = samp_rate = 8e6
        self.inter = inter = 64
        self.guard = guard = 1/16.0
        self.decim = decim = 63
        self.data_carriers = data_carriers = 13*96*2**(mode-1)
        self.center_freq = center_freq = 629.143e6
        self.btn_prev = btn_prev = 0
        self.btn_play = btn_play = 0
        self.btn_next = btn_next = 0
        self.active_carriers = active_carriers = 13*108*2**(mode-1)+1

        ##################################################
        # Blocks
        ##################################################
        self._samp_rate_tool_bar = Qt.QToolBar(self)
        self._samp_rate_tool_bar.addWidget(Qt.QLabel('Samp Rate'+": "))
        self._samp_rate_line_edit = Qt.QLineEdit(str(self.samp_rate))
        self._samp_rate_tool_bar.addWidget(self._samp_rate_line_edit)
        self._samp_rate_line_edit.returnPressed.connect(
        	lambda: self.set_samp_rate(eng_notation.str_to_num(str(self._samp_rate_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._samp_rate_tool_bar, 1, 13, 1, 2)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._guard_options = (1/4.0, 1/8.0, 1/16.0, 1/32.0, )
        self._guard_labels = ('1/4', '1/8', '1/16', '1/32', )
        self._guard_tool_bar = Qt.QToolBar(self)
        self._guard_tool_bar.addWidget(Qt.QLabel('Guard Interval'+": "))
        self._guard_combo_box = Qt.QComboBox()
        self._guard_tool_bar.addWidget(self._guard_combo_box)
        for label in self._guard_labels: self._guard_combo_box.addItem(label)
        self._guard_callback = lambda i: Qt.QMetaObject.invokeMethod(self._guard_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._guard_options.index(i)))
        self._guard_callback(self.guard)
        self._guard_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_guard(self._guard_options[i]))
        self.top_grid_layout.addWidget(self._guard_tool_bar, 2, 13, 1, 2)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_time_sink_vi = qtgui.time_sink_f(
        	total_carriers*24, #size
        	samp_rate, #samp_rate
        	'Viterbi', #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_vi.set_update_time(0.10)
        self.qtgui_time_sink_vi.set_y_axis(-9, 0)

        self.qtgui_time_sink_vi.set_y_label('Amplitude', "")

        self.qtgui_time_sink_vi.enable_tags(-1, False)
        self.qtgui_time_sink_vi.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_vi.enable_autoscale(False)
        self.qtgui_time_sink_vi.enable_grid(False)
        self.qtgui_time_sink_vi.enable_axis_labels(True)
        self.qtgui_time_sink_vi.enable_control_panel(False)
        self.qtgui_time_sink_vi.enable_stem_plot(False)

        if not False:
          self.qtgui_time_sink_vi.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_vi.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_vi.set_line_label(i, labels[i])
            self.qtgui_time_sink_vi.set_line_width(i, widths[i])
            self.qtgui_time_sink_vi.set_line_color(i, colors[i])
            self.qtgui_time_sink_vi.set_line_style(i, styles[i])
            self.qtgui_time_sink_vi.set_line_marker(i, markers[i])
            self.qtgui_time_sink_vi.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_vi_win = sip.wrapinstance(self.qtgui_time_sink_vi.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_vi_win, 6, 6, 6, 6)
        for r in range(6, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(6, 12):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_time_sink_rs = qtgui.time_sink_f(
        	total_carriers, #size
        	samp_rate, #samp_rate
        	'Reed-Solomon', #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_rs.set_update_time(0.10)
        self.qtgui_time_sink_rs.set_y_axis(-9, 0)

        self.qtgui_time_sink_rs.set_y_label('Amplitude', "")

        self.qtgui_time_sink_rs.enable_tags(-1, False)
        self.qtgui_time_sink_rs.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_rs.enable_autoscale(True)
        self.qtgui_time_sink_rs.enable_grid(False)
        self.qtgui_time_sink_rs.enable_axis_labels(True)
        self.qtgui_time_sink_rs.enable_control_panel(False)
        self.qtgui_time_sink_rs.enable_stem_plot(False)

        if not False:
          self.qtgui_time_sink_rs.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_rs.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_rs.set_line_label(i, labels[i])
            self.qtgui_time_sink_rs.set_line_width(i, widths[i])
            self.qtgui_time_sink_rs.set_line_color(i, colors[i])
            self.qtgui_time_sink_rs.set_line_style(i, styles[i])
            self.qtgui_time_sink_rs.set_line_marker(i, markers[i])
            self.qtgui_time_sink_rs.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_rs_win = sip.wrapinstance(self.qtgui_time_sink_rs.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_rs_win, 0, 6, 6, 6)
        for r in range(0, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(6, 12):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_const_sink = qtgui.const_sink_c(
        	active_carriers, #size
        	'Constellation', #name
        	1 #number of inputs
        )
        self.qtgui_const_sink.set_update_time(0.10)
        self.qtgui_const_sink.set_y_axis(-2, 2)
        self.qtgui_const_sink.set_x_axis(-2, 2)
        self.qtgui_const_sink.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, "")
        self.qtgui_const_sink.enable_autoscale(False)
        self.qtgui_const_sink.enable_grid(False)
        self.qtgui_const_sink.enable_axis_labels(True)

        if not False:
          self.qtgui_const_sink.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "red", "red", "red",
                  "red", "red", "red", "red", "red"]
        styles = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        markers = [0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_const_sink.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_const_sink.set_line_label(i, labels[i])
            self.qtgui_const_sink.set_line_width(i, widths[i])
            self.qtgui_const_sink.set_line_color(i, colors[i])
            self.qtgui_const_sink.set_line_style(i, styles[i])
            self.qtgui_const_sink.set_line_marker(i, markers[i])
            self.qtgui_const_sink.set_line_alpha(i, alphas[i])

        self._qtgui_const_sink_win = sip.wrapinstance(self.qtgui_const_sink.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_const_sink_win, 0, 0, 12, 6)
        for r in range(0, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 6):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._mode_options = (3, 2, 1, )
        self._mode_labels = ('3 (8k)', '2 (4k)', '1 (2k)', )
        self._mode_tool_bar = Qt.QToolBar(self)
        self._mode_tool_bar.addWidget(Qt.QLabel('Mode'+": "))
        self._mode_combo_box = Qt.QComboBox()
        self._mode_tool_bar.addWidget(self._mode_combo_box)
        for label in self._mode_labels: self._mode_combo_box.addItem(label)
        self._mode_callback = lambda i: Qt.QMetaObject.invokeMethod(self._mode_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._mode_options.index(i)))
        self._mode_callback(self.mode)
        self._mode_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_mode(self._mode_options[i]))
        self.top_grid_layout.addWidget(self._mode_tool_bar, 3, 13, 1, 2)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.low_pass_filter_src = filter.fir_filter_ccf(1, firdes.low_pass(
        	1, samp_rate, 5.8e6/2.0, 0.5e6, firdes.WIN_HAMMING, 6.76))
        self.isdbt_viterbi_decoder = isdbt.viterbi_decoder(64, 2)
        self.isdbt_tmcc_decoder = isdbt.tmcc_decoder(3, False)
        self.isdbt_time_deinterleaver = isdbt.time_deinterleaver(3, 1, 4, 12, 2, 0, 0)
        self.isdbt_sync_and_channel_estimation = isdbt.sync_and_channel_estimation(8192, 5617, 10)
        self.isdbt_symbol_demapper = isdbt.symbol_demapper(3, 1, 4, 12, 64, 0, 64)
        self.isdbt_reed_solomon_dec_isdbt_0 = isdbt.reed_solomon_dec_isdbt()
        self.isdbt_ofdm_sym_acquisition = isdbt.ofdm_sym_acquisition(total_carriers, int(guard*total_carriers), 10)
        self.isdbt_frequency_deinterleaver = isdbt.frequency_deinterleaver(True, 3)
        self.isdbt_energy_descrambler_0 = isdbt.energy_descrambler()
        self.isdbt_byte_deinterleaver_0 = isdbt.byte_deinterleaver()
        self.isdbt_bit_deinterleaver = isdbt.bit_deinterleaver(3, 12, 64)
        self.fft_vxx_rx = fft.fft_vcc(total_carriers, True, (window.rectangular(total_carriers)), True, 1)
        self._center_freq_tool_bar = Qt.QToolBar(self)
        self._center_freq_tool_bar.addWidget(Qt.QLabel('Center Freq'+": "))
        self._center_freq_line_edit = Qt.QLineEdit(str(self.center_freq))
        self._center_freq_tool_bar.addWidget(self._center_freq_line_edit)
        self._center_freq_line_edit.returnPressed.connect(
        	lambda: self.set_center_freq(eng_notation.str_to_num(str(self._center_freq_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._center_freq_tool_bar, 0, 13, 1, 2)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        _btn_prev_push_button = Qt.QPushButton('Channel (-)')
        self._btn_prev_choices = {'Pressed': 1, 'Released': 0}
        _btn_prev_push_button.pressed.connect(lambda: self.set_btn_prev(self._btn_prev_choices['Pressed']))
        _btn_prev_push_button.released.connect(lambda: self.set_btn_prev(self._btn_prev_choices['Released']))
        self.top_grid_layout.addWidget(_btn_prev_push_button, 10, 13, 1, 1)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 14):
            self.top_grid_layout.setColumnStretch(c, 1)
        _btn_play_push_button = Qt.QPushButton('Play')
        self._btn_play_choices = {'Pressed': 1, 'Released': 0}
        _btn_play_push_button.pressed.connect(lambda: self.set_btn_play(self._btn_play_choices['Pressed']))
        _btn_play_push_button.released.connect(lambda: self.set_btn_play(self._btn_play_choices['Released']))
        self.top_grid_layout.addWidget(_btn_play_push_button, 11, 13, 1, 2)
        for r in range(11, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        _btn_next_push_button = Qt.QPushButton('Channel (+)')
        self._btn_next_choices = {'Pressed': 1, 'Released': 0}
        _btn_next_push_button.pressed.connect(lambda: self.set_btn_next(self._btn_next_choices['Pressed']))
        _btn_next_push_button.released.connect(lambda: self.set_btn_next(self._btn_next_choices['Released']))
        self.top_grid_layout.addWidget(_btn_next_push_button, 10, 14, 1, 1)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(14, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_vector_to_stream_rx = blocks.vector_to_stream(gr.sizeof_gr_complex*1, data_carriers)
        self.blocks_vector_to_stream_rs = blocks.vector_to_stream(gr.sizeof_char*1, 188)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_char*384)
        self.blocks_nlog10_ff_0_0 = blocks.nlog10_ff(1, 1, 0)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(1, 1, 0)
        self.blocks_file_source_1 = blocks.file_source(gr.sizeof_gr_complex*1, '/home/ergwd/Projects/gr-miind/flowgraphs/miind-tv/sample-recordings/569MHz_recording.dat', False)
        self.blocks_file_source_1.set_begin_tag(pmt.PMT_NIL)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_char*1, '/home/ergwd/Projects/gr-miind/flowgraphs/miind-tv/output-recordings/test_out.ts', False)
        self.blocks_file_sink_0.set_unbuffered(False)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_file_source_1, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.qtgui_time_sink_rs, 0))
        self.connect((self.blocks_nlog10_ff_0_0, 0), (self.qtgui_time_sink_vi, 0))
        self.connect((self.blocks_throttle_0, 0), (self.low_pass_filter_src, 0))
        self.connect((self.blocks_vector_to_stream_rs, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.blocks_vector_to_stream_rx, 0), (self.qtgui_const_sink, 0))
        self.connect((self.fft_vxx_rx, 0), (self.isdbt_sync_and_channel_estimation, 0))
        self.connect((self.isdbt_bit_deinterleaver, 0), (self.isdbt_viterbi_decoder, 0))
        self.connect((self.isdbt_byte_deinterleaver_0, 0), (self.isdbt_energy_descrambler_0, 0))
        self.connect((self.isdbt_energy_descrambler_0, 0), (self.isdbt_reed_solomon_dec_isdbt_0, 0))
        self.connect((self.isdbt_frequency_deinterleaver, 0), (self.isdbt_time_deinterleaver, 0))
        self.connect((self.isdbt_ofdm_sym_acquisition, 0), (self.fft_vxx_rx, 0))
        self.connect((self.isdbt_reed_solomon_dec_isdbt_0, 1), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.isdbt_reed_solomon_dec_isdbt_0, 0), (self.blocks_vector_to_stream_rs, 0))
        self.connect((self.isdbt_symbol_demapper, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.isdbt_symbol_demapper, 1), (self.isdbt_bit_deinterleaver, 0))
        self.connect((self.isdbt_sync_and_channel_estimation, 0), (self.isdbt_tmcc_decoder, 0))
        self.connect((self.isdbt_time_deinterleaver, 0), (self.isdbt_symbol_demapper, 0))
        self.connect((self.isdbt_tmcc_decoder, 0), (self.blocks_vector_to_stream_rx, 0))
        self.connect((self.isdbt_tmcc_decoder, 0), (self.isdbt_frequency_deinterleaver, 0))
        self.connect((self.isdbt_viterbi_decoder, 1), (self.blocks_nlog10_ff_0_0, 0))
        self.connect((self.isdbt_viterbi_decoder, 0), (self.isdbt_byte_deinterleaver_0, 0))
        self.connect((self.low_pass_filter_src, 0), (self.isdbt_ofdm_sym_acquisition, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "isdbt_rx_live")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_mode(self):
        return self.mode

    def set_mode(self, mode):
        self.mode = mode
        self.set_total_carriers(2**(10+self.mode))
        self.set_data_carriers(13*96*2**(self.mode-1))
        self.set_active_carriers(13*108*2**(self.mode-1)+1)
        self._mode_callback(self.mode)

    def get_total_carriers(self):
        return self.total_carriers

    def set_total_carriers(self, total_carriers):
        self.total_carriers = total_carriers

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        Qt.QMetaObject.invokeMethod(self._samp_rate_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.samp_rate)))
        self.qtgui_time_sink_vi.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_rs.set_samp_rate(self.samp_rate)
        self.low_pass_filter_src.set_taps(firdes.low_pass(1, self.samp_rate, 5.8e6/2.0, 0.5e6, firdes.WIN_HAMMING, 6.76))
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_inter(self):
        return self.inter

    def set_inter(self, inter):
        self.inter = inter

    def get_guard(self):
        return self.guard

    def set_guard(self, guard):
        self.guard = guard
        self._guard_callback(self.guard)

    def get_decim(self):
        return self.decim

    def set_decim(self, decim):
        self.decim = decim

    def get_data_carriers(self):
        return self.data_carriers

    def set_data_carriers(self, data_carriers):
        self.data_carriers = data_carriers

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        Qt.QMetaObject.invokeMethod(self._center_freq_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.center_freq)))

    def get_btn_prev(self):
        return self.btn_prev

    def set_btn_prev(self, btn_prev):
        self.btn_prev = btn_prev

    def get_btn_play(self):
        return self.btn_play

    def set_btn_play(self, btn_play):
        self.btn_play = btn_play

    def get_btn_next(self):
        return self.btn_next

    def set_btn_next(self, btn_next):
        self.btn_next = btn_next

    def get_active_carriers(self):
        return self.active_carriers

    def set_active_carriers(self, active_carriers):
        self.active_carriers = active_carriers


def main(top_block_cls=isdbt_rx_live, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
