#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Satellite Receiver
# Author: Daryl Pongcol
# GNU Radio version: 3.7.13.4
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
from qt_freq_wfall_sink import qt_freq_wfall_sink  # grc-generated hier_block
import time
from gnuradio import qtgui


class main_sat_rex(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Satellite Receiver")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Satellite Receiver")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "main_sat_rex")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.update_period = update_period = 0.1
        self.satellite = satellite = "NOAA 18"
        self.samp_rate = samp_rate = 5e6
        self.num_points = num_points = int(8e3)
        self.fft_size = fft_size = int(4e3)
        self.center_freq = center_freq = 100e6
        self.btn_prev = btn_prev = 0
        self.btn_next = btn_next = 0
        self.btn_capture_sat = btn_capture_sat = 0
        self.btn_capture_all = btn_capture_all = 0
        self.bandwidth = bandwidth = int(20e6)

        ##################################################
        # Blocks
        ##################################################
        self._update_period_tool_bar = Qt.QToolBar(self)
        self._update_period_tool_bar.addWidget(Qt.QLabel('Update Period'+": "))
        self._update_period_line_edit = Qt.QLineEdit(str(self.update_period))
        self._update_period_tool_bar.addWidget(self._update_period_line_edit)
        self._update_period_line_edit.returnPressed.connect(
        	lambda: self.set_update_period(eng_notation.str_to_num(str(self._update_period_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._update_period_tool_bar, 5, 13, 1, 2)
        for r in range(5, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._samp_rate_tool_bar = Qt.QToolBar(self)
        self._samp_rate_tool_bar.addWidget(Qt.QLabel('Samp Rate'+": "))
        self._samp_rate_line_edit = Qt.QLineEdit(str(self.samp_rate))
        self._samp_rate_tool_bar.addWidget(self._samp_rate_line_edit)
        self._samp_rate_line_edit.returnPressed.connect(
        	lambda: self.set_samp_rate(eng_notation.str_to_num(str(self._samp_rate_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._samp_rate_tool_bar, 1, 13, 1, 2)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._num_points_tool_bar = Qt.QToolBar(self)
        self._num_points_tool_bar.addWidget(Qt.QLabel('# of Points'+": "))
        self._num_points_line_edit = Qt.QLineEdit(str(self.num_points))
        self._num_points_tool_bar.addWidget(self._num_points_line_edit)
        self._num_points_line_edit.returnPressed.connect(
        	lambda: self.set_num_points(int(str(self._num_points_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._num_points_tool_bar, 3, 13, 1, 2)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._fft_size_tool_bar = Qt.QToolBar(self)
        self._fft_size_tool_bar.addWidget(Qt.QLabel('FFT Size'+": "))
        self._fft_size_line_edit = Qt.QLineEdit(str(self.fft_size))
        self._fft_size_tool_bar.addWidget(self._fft_size_line_edit)
        self._fft_size_line_edit.returnPressed.connect(
        	lambda: self.set_fft_size(int(str(self._fft_size_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._fft_size_tool_bar, 4, 13, 1, 2)
        for r in range(4, 5):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._center_freq_tool_bar = Qt.QToolBar(self)
        self._center_freq_tool_bar.addWidget(Qt.QLabel('Center Freq'+": "))
        self._center_freq_line_edit = Qt.QLineEdit(str(self.center_freq))
        self._center_freq_tool_bar.addWidget(self._center_freq_line_edit)
        self._center_freq_line_edit.returnPressed.connect(
        	lambda: self.set_center_freq(eng_notation.str_to_num(str(self._center_freq_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._center_freq_tool_bar, 0, 13, 1, 2)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._bandwidth_tool_bar = Qt.QToolBar(self)
        self._bandwidth_tool_bar.addWidget(Qt.QLabel('Bandwidth'+": "))
        self._bandwidth_line_edit = Qt.QLineEdit(str(self.bandwidth))
        self._bandwidth_tool_bar.addWidget(self._bandwidth_line_edit)
        self._bandwidth_line_edit.returnPressed.connect(
        	lambda: self.set_bandwidth(int(str(self._bandwidth_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._bandwidth_tool_bar, 2, 13, 1, 2)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.uhd_usrp_source = uhd.usrp_source(
        	",".join(("", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source.set_samp_rate(samp_rate)
        self.uhd_usrp_source.set_center_freq(int(center_freq), 0)
        self.uhd_usrp_source.set_gain(30, 0)
        self.uhd_usrp_source.set_bandwidth(int(bandwidth), 0)
        self.uhd_usrp_source.set_auto_dc_offset(True, 0)
        self.uhd_usrp_source.set_auto_iq_balance(True, 0)
        self._satellite_options = ("NOAA 15", "NOAA 18", "NOAA 19", "METEOR-M 2", )
        self._satellite_labels = ('NOAA 15', 'NOAA 18', 'NOAA 19', 'METEOR-M 2', )
        self._satellite_group_box = Qt.QGroupBox('Satellite')
        self._satellite_box = Qt.QVBoxLayout()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._satellite_button_group = variable_chooser_button_group()
        self._satellite_group_box.setLayout(self._satellite_box)
        for i, label in enumerate(self._satellite_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._satellite_box.addWidget(radio_button)
        	self._satellite_button_group.addButton(radio_button, i)
        self._satellite_callback = lambda i: Qt.QMetaObject.invokeMethod(self._satellite_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._satellite_options.index(i)))
        self._satellite_callback(self.satellite)
        self._satellite_button_group.buttonClicked[int].connect(
        	lambda i: self.set_satellite(self._satellite_options[i]))
        self.top_grid_layout.addWidget(self._satellite_group_box, 6, 13, 1, 2)
        for r in range(6, 7):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qt_freq_wfall_sink = qt_freq_wfall_sink(
            bandwidth=bandwidth,
            center_freq=center_freq,
            fft_size=fft_size,
            num_points=num_points,
            samp_rate=samp_rate,
            update_period=update_period,
        )
        self.top_grid_layout.addWidget(self.qt_freq_wfall_sink, 0, 0, 13, 12)
        for r in range(0, 13):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 12):
            self.top_grid_layout.setColumnStretch(c, 1)
        _btn_prev_push_button = Qt.QPushButton('Channel (-)')
        self._btn_prev_choices = {'Pressed': 1, 'Released': 0}
        _btn_prev_push_button.pressed.connect(lambda: self.set_btn_prev(self._btn_prev_choices['Pressed']))
        _btn_prev_push_button.released.connect(lambda: self.set_btn_prev(self._btn_prev_choices['Released']))
        self.top_grid_layout.addWidget(_btn_prev_push_button, 10, 13, 1, 1)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 14):
            self.top_grid_layout.setColumnStretch(c, 1)
        _btn_next_push_button = Qt.QPushButton('Channel (+)')
        self._btn_next_choices = {'Pressed': 1, 'Released': 0}
        _btn_next_push_button.pressed.connect(lambda: self.set_btn_next(self._btn_next_choices['Pressed']))
        _btn_next_push_button.released.connect(lambda: self.set_btn_next(self._btn_next_choices['Released']))
        self.top_grid_layout.addWidget(_btn_next_push_button, 10, 14, 1, 1)
        for r in range(10, 11):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(14, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        _btn_capture_sat_push_button = Qt.QPushButton('Capture Satellite')
        self._btn_capture_sat_choices = {'Pressed': 1, 'Released': 0}
        _btn_capture_sat_push_button.pressed.connect(lambda: self.set_btn_capture_sat(self._btn_capture_sat_choices['Pressed']))
        _btn_capture_sat_push_button.released.connect(lambda: self.set_btn_capture_sat(self._btn_capture_sat_choices['Released']))
        self.top_grid_layout.addWidget(_btn_capture_sat_push_button, 11, 13, 1, 2)
        for r in range(11, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        _btn_capture_all_push_button = Qt.QPushButton('Capture All')
        self._btn_capture_all_choices = {'Pressed': 1, 'Released': 0}
        _btn_capture_all_push_button.pressed.connect(lambda: self.set_btn_capture_all(self._btn_capture_all_choices['Pressed']))
        _btn_capture_all_push_button.released.connect(lambda: self.set_btn_capture_all(self._btn_capture_all_choices['Released']))
        self.top_grid_layout.addWidget(_btn_capture_all_push_button, 12, 13, 1, 2)
        for r in range(12, 13):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_throttle_0, 0), (self.qt_freq_wfall_sink, 0))
        self.connect((self.uhd_usrp_source, 0), (self.blocks_throttle_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "main_sat_rex")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_update_period(self):
        return self.update_period

    def set_update_period(self, update_period):
        self.update_period = update_period
        Qt.QMetaObject.invokeMethod(self._update_period_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.update_period)))
        self.qt_freq_wfall_sink.set_update_period(self.update_period)

    def get_satellite(self):
        return self.satellite

    def set_satellite(self, satellite):
        self.satellite = satellite
        self._satellite_callback(self.satellite)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        Qt.QMetaObject.invokeMethod(self._samp_rate_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.samp_rate)))
        self.uhd_usrp_source.set_samp_rate(self.samp_rate)
        self.qt_freq_wfall_sink.set_samp_rate(self.samp_rate)
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_num_points(self):
        return self.num_points

    def set_num_points(self, num_points):
        self.num_points = num_points
        Qt.QMetaObject.invokeMethod(self._num_points_line_edit, "setText", Qt.Q_ARG("QString", str(self.num_points)))
        self.qt_freq_wfall_sink.set_num_points(self.num_points)

    def get_fft_size(self):
        return self.fft_size

    def set_fft_size(self, fft_size):
        self.fft_size = fft_size
        Qt.QMetaObject.invokeMethod(self._fft_size_line_edit, "setText", Qt.Q_ARG("QString", str(self.fft_size)))
        self.qt_freq_wfall_sink.set_fft_size(self.fft_size)

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        Qt.QMetaObject.invokeMethod(self._center_freq_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.center_freq)))
        self.uhd_usrp_source.set_center_freq(int(self.center_freq), 0)
        self.qt_freq_wfall_sink.set_center_freq(self.center_freq)

    def get_btn_prev(self):
        return self.btn_prev

    def set_btn_prev(self, btn_prev):
        self.btn_prev = btn_prev

    def get_btn_next(self):
        return self.btn_next

    def set_btn_next(self, btn_next):
        self.btn_next = btn_next

    def get_btn_capture_sat(self):
        return self.btn_capture_sat

    def set_btn_capture_sat(self, btn_capture_sat):
        self.btn_capture_sat = btn_capture_sat

    def get_btn_capture_all(self):
        return self.btn_capture_all

    def set_btn_capture_all(self, btn_capture_all):
        self.btn_capture_all = btn_capture_all

    def get_bandwidth(self):
        return self.bandwidth

    def set_bandwidth(self, bandwidth):
        self.bandwidth = bandwidth
        Qt.QMetaObject.invokeMethod(self._bandwidth_line_edit, "setText", Qt.Q_ARG("QString", str(self.bandwidth)))
        self.uhd_usrp_source.set_bandwidth(int(self.bandwidth), 0)
        self.qt_freq_wfall_sink.set_bandwidth(self.bandwidth)


def main(top_block_cls=main_sat_rex, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
