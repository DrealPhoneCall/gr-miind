import os
from subprocess import call
from variables.params import get_channels, get_param_json

class mixin_buttons():
    def set_btn_prev(self, btn_prev):
        if btn_prev:
            print "btn_prev callback"
            try:
                self.current_channel_index -= 1
                channel = self.channels[self.current_channel_index]
            except:
                self.current_channel_index = len(self.channels)-1
                channel = self.channels[self.current_channel_index]
            self.variables = get_param_json(channel)
            self.set_channel_params()

    def set_btn_next(self, btn_next):
        if btn_next:
            print "btn_next callback"
            try:
                self.current_channel_index += 1
                channel = self.channels[self.current_channel_index]
            except:
                self.current_channel_index = 0
                channel = self.channels[self.current_channel_index]
            self.variables = get_param_json(channel)
            self.set_channel_params()

    def set_btn_capture_sat(self, btn_capture_sat):
        cwd = os.getcwd()
        bash_file = '%s/flowgraphs/miind-sat/weather/predict/schedule_satellite.sh' %(cwd)
        print "btn_capture_sat callback"
        call([bash_file, str(self.satellite), str(self.center_freq/1e6)]) 

    def set_btn_capture_all(self, btn_capture_all):
        print "btn_capture_all callback"
        cwd = os.getcwd()
        bash_file = '%s/flowgraphs/miind-sat/weather/predict/schedule_all.sh' %(cwd)
        call([bash_file])

class mixin_params():
    def __init__(self):
        self.channels = get_channels()
        self.current_channel_index = 0
        self.variables = get_param_json('channel_01')

    def set_channel_params(self):
        self.set_center_freq(int(self.variables['center_freq']))
        self.set_samp_rate(int(self.variables['samp_rate']))
        self.set_bandwidth(int(self.variables['bandwidth']))
        self.set_num_points(self.variables['fft_size'])
        self.set_update_period(self.variables['update_period'])
        self.set_satellite(self.variables['satellite'])

    