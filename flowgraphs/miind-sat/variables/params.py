import os
import json

def get_channels():
    return ['channel_01', 'channel_02', 'channel_03']

def get_param_json(channel):
    cwd = os.getcwd()
    file_path = '%s/flowgraphs/miind-sat/variables/%s.json' % (cwd, channel)
    with open(file_path) as json_data:
        json_param = json.load(json_data)
        print json_param
    return json_param