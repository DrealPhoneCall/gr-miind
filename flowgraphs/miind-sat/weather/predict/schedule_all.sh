#!/bin/bash

# Update Satellite Information

satrexpath=~/Projects/gr-miind/flowgraphs/miind-sat

wget -qr https://www.celestrak.com/NORAD/elements/weather.txt -O $satrexpath/weather/predict/weather.txt
grep "NOAA 15" $satrexpath/weather/predict/weather.txt -A 2 > $satrexpath/weather/predict/weather.tle
grep "NOAA 18" $satrexpath/weather/predict/weather.txt -A 2 >> $satrexpath/weather/predict/weather.tle
grep "NOAA 19" $satrexpath/weather/predict/weather.txt -A 2 >> $satrexpath/weather/predict/weather.tle
grep "METEOR-M 2" $satrexpath/weather/predict/weather.txt -A 2 >> $satrexpath/weather/predict/weather.tle



#Remove all AT jobs

for i in `atq | awk '{print $1}'`;do atrm $i;done


#Schedule Satellite Passes:

$satrexpath/weather/predict/schedule_satellite.sh "NOAA 19" 137.1000
$satrexpath/weather/predict/schedule_satellite.sh "NOAA 18" 137.9125
$satrexpath/weather/predict/schedule_satellite.sh "NOAA 15" 137.6200