#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: GPL-3.0
#
##################################################
# GNU Radio Python Flow Graph
# Title: miind | ISDBT Obtain Parameters
# Generated: Fri Nov  2 22:02:39 2018
# GNU Radio version: 3.7.12.0
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import gr
from gnuradio import qtgui

from main_sat_rex import main_sat_rex
from mixins import mixin_buttons, mixin_params


class main_rx_live(main_sat_rex, mixin_buttons, mixin_params):

    def __init__(self):
        main_sat_rex.__init__(self)
        mixin_params.__init__(self)

    def set_satellite(self, satellite):
        main_sat_rex.set_satellite(self, satellite)

    def set_channel_params(self):
        mixin_params.set_channel_params(self)

    def set_btn_prev(self, btn_prev):
        main_sat_rex.set_btn_prev(self, btn_prev)
        mixin_buttons.set_btn_prev(self, btn_prev)

    def set_btn_next(self, btn_next):
        main_sat_rex.set_btn_next(self, btn_next)
        mixin_buttons.set_btn_next(self, btn_next)

    def set_btn_capture_sat(self, btn_capture_sat):
        main_sat_rex.set_btn_capture_sat(self, btn_capture_sat)
        mixin_buttons.set_btn_capture_sat(self, btn_capture_sat)

    def set_btn_capture_all(self, btn_capture_all):
        main_sat_rex.set_btn_capture_all(self, btn_capture_all)
        mixin_buttons.set_btn_capture_all(self, btn_capture_all)


def main(top_block_cls=main_rx_live, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
