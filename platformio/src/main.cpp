/*

  Standard analog read for miindProbe

  Daryl Pongcol

*/

#include "Arduino.h"

#define BAUD_RATE 115200
#define NUM_PINS 1
#define START_PIN 0

double valRead;
double valOut;

void setup(){
  // initialize serial comms
  Serial.begin(BAUD_RATE); 
}

void loop(){
  
  // read piezo pin
  for(unsigned short i=START_PIN; i<START_PIN+NUM_PINS; i++){
    valRead = analogRead(i);
    valOut = valRead * 5.0 / 1024.0;
    Serial.println(valOut);
  }

  // wait 
  delayMicroseconds(100);
}