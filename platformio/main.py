import shutil
from settings.args import arg_parser
 
"""
    cd first into the main.py directory.
    then run (windows and mac):
        python main.py -t arduino_std -mp "berdrums 1" -cp COM6 -br 31250 -pc ad_rock.json
        python src/main.py -t arduino_std -mp "IAC Driver berdrums 1" -cp /dev/cu.wchusbserial1440 -mc 0 -br 31250 -pc ad_rock.json
"""

 
def main():
    parser = arg_parser()
    args = parser.parse_args()
    
    try:
        if args.list_devices:
            import sounddevice as sd
            print(sd.query_devices())
            parser.exit(0)

        if args.list_midi_ports:
            import mido
            mido_ports = mido.get_output_names()
            for port in mido_ports:
                print(port)
            parser.exit(0)
        
        if args.list_serial_ports:
            from subprocess import call
            call(["python", "-m", "serial.tools.list_ports"])
            parser.exit(0)

        if args.multi_serial_port:
            from arduino_std.main_new import Main
            main = Main(args.multi_serial_port, False, args.perc_slug, args.com_port, args.midi_port, 
                        args.midi_channel, args.baud_rate, 
                        args.pad_config, args.output_msg_type, show_latency=True)
            main.read_multi_port()
        elif args.listen_to_socket:
            from arduino_std.main_new import Main
            main = Main(args.multi_serial_port, True, args.perc_slug, args.com_port, args.midi_port, 
                        args.midi_channel, args.baud_rate, 
                        args.pad_config, args.output_msg_type, show_latency=True)
            main.listen_to_socket()
        else:
            # run drum pad modules
            if args.type == 'arduino_std':
                import arduino_std.main as arduino_std
                if args.show_latency:
                    arduino_std.main(args.perc_slug, args.com_port, args.midi_port, 
                                    args.midi_channel, args.baud_rate, 
                                    args.pad_config, args.output_msg_type, show_latency=True)
                else:
                    arduino_std.main(args.perc_slug, args.com_port, args.midi_port, 
                                    args.midi_channel, args.baud_rate, 
                                    args.pad_config, args.output_msg_type)

        if args.type == 'arduino_firmata':
            import arduino_firmata.main as arduino_firmata
            arduino_firmata.main(args.com_port, args.midi_port, 
                                args.pad_config)

        if args.type == 'layered_triggers':
            import layered_triggers.main as layered_triggers 
            layered_triggers.main(args.com_port, args.midi_port, 
                                args.baud_rate, args.freq_config)

    except KeyboardInterrupt:
        parser.exit('\nInterrupted by user')
    except Exception as e:
        parser.exit(type(e).__name__ + ': ' + str(e))
 
if __name__ == '__main__':
    main()
        