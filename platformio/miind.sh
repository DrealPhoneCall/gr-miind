#! /bin/bash

# If permission is denied, run this:
# chmod +x miind.sh

# Get the arguments
command=$1
board=$2

if [[ "$command" == "src" ]]; then
    file=$2
    echo "Executing miind.sh src command..."
    cp "./src_firmware/$file" "src/main.cpp"
elif [[ "$command" == "build" ]]; then
    echo "Executing platformio run command..."
    if [[ "$board" == "uno" ]]; then
        echo "Compiling code for arduino uno..."
        pio run -e uno
    elif [[ "$board" == "nano" ]]; then
        echo "Compiling code for arduino nano..."
        pio run -e nanoatmega328
    fi
elif [[ "$command" == "upload" ]]; then
    echo "Executing platformio upload command..."
    if [[ "$board" == "uno" ]]; then
        echo "Uploading code for arduino uno..."
        pio run -e uno --target upload
    elif [[ "$board" == "nano" ]]; then
        echo "Uploading code for arduino nano..."
        pio run -e nanoatmega328 --target upload
    fi
elif [[ "$command" == "monitor" ]]; then
    echo "Executing platformio device monitor..."
    pio device monitor    

elif [[ "$command" == "stty" ]]; then
    echo "Executing stty on port..."
    serial_port=$2
    baud_rate=$3
    sudo stty -F $serial_port $baud_rate
elif [[ "$command" == "permit-port" ]]; then
    echo "Executing chmod a+rw on port..."
    serial_port=$2
    sudo chmod a+rw $serial_port
fi
