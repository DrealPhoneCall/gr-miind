/*

  Standard analog read for miindScope

  Daryl Pongcol

*/

#include "Arduino.h"

#define BAUD_RATE 115200
#define NUM_PINS 4
#define START_PIN 0

void setup(){
  // initialize serial comms
  Serial.begin(BAUD_RATE); 
}

void loop(){
  
  for(unsigned short i=START_PIN; i<START_PIN+NUM_PINS; i++){
    // read piezo pin
    unsigned short valRead = analogRead(i);
    double valOut = valRead * 5.0 / 1024.0;
    Serial.println(valOut);
    
  }

  // wait 
  delay(100);
}